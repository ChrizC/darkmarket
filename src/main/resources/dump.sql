CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(16) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT 0,
  `world` varchar(64) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `z` int(11) NOT NULL,
  `buy` double(11,2) NOT NULL,
  `sell` double(11,2) NOT NULL,
  `quantity` smallint(5) NOT NULL,
  `type` smallint(4) NOT NULL,
  `durability` smallint(5) NOT NULL,
  `meta_id` int(11) DEFAULT -1,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `chests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `world` varchar(64) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `z` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`shop_id`,`world`,`x`,`y`,`z`)
) ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bytes` BLOB NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop` varchar(16) NOT NULL,
  `client` varchar(16) NOT NULL,
  `buy` tinyint(1) NOT NULL,
  `price` double(10,2) NOT NULL,
  `quantity` smallint(5) NOT NULL,
  `type` smallint(4) NOT NULL,
  `data` smallint(5) NOT NULL,
  `enchantments` varchar(255) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB;