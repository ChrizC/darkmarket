package org.darkcraft.darkmarket.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import org.darkcraft.darkmarket.Config;
import org.darkcraft.darkmarket.DarkMarket;

public class Database {
    
    private static String url = "jdbc:mysql://" + Config.mysqlHostName + ":" + Config.mysqlPort + "/" + Config.mysqlDatabase;
    private static String username = Config.mysqlUsername;
    private static String password = Config.mysqlPassword;
    
    private Connection connection;
    
    private static Database instance;
    
    public static Database inst() {
        if (instance == null) {
            instance = new Database();
        }
        return instance;
    }
    
    public void open() throws SQLException {
        close();
        connection = getNewConnection();
    }
    
    public void close() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }
    
    public Connection getConnection() {
        return connection;
    }
    
    public void executeQuery(String... query) {
        try (Statement stmt = getStatement()) {
            for (String statement : query) {
                stmt.execute(statement);
            }
        } catch (SQLException ex) {
            DarkMarket.LOG.log(Level.SEVERE, null, ex);
        }
    }
    
    public static Connection getNewConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }
    
    public Statement getStatement() {
        try {
            return getConnection().createStatement();
        } catch (SQLException ex) {
            DarkMarket.LOG.log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public PreparedStatement prepareStatement(String sql, boolean keys) {
        try {
            return getConnection().prepareStatement(sql, keys? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
        } catch (SQLException ex) {
            DarkMarket.LOG.log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}
