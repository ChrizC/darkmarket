package org.darkcraft.darkmarket.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.darkcraft.darkmarket.DarkMarket;
import org.darkcraft.darkmarket.inventory.MultiChestInventory;
import org.darkcraft.darkmarket.shop.Shop;
import org.darkcraft.darkmarket.utils.uBlock;

public class Chests {

    private static final String SELECT_SHOP =
            "SELECT shop_id FROM chests WHERE world = '%s' AND x = %d AND y = %d AND z = %d";
    private static final String SELECT_ALL =
            "SELECT world, x, y, z FROM chests WHERE shop_id = %d";
    private static final String DELETE =
            "DELETE FROM chests WHERE shop_id = %d";
    private static final String DELETE_MULTIPLE = "DELETE FROM chests WHERE (shop_id, world, x, y, z) IN (%s)";
    private static final String INSERT =
            "INSERT INTO chests (shop_id, world, x, y, z) VALUES ";

    // Clears all chests associated with a shop
    public static void clearShopChests(Shop shop) {
        clearShopChests(shop.getId());
    }

    public static void clearShopChests(int shopId) {
        Queue.queueQuery(String.format(DELETE, shopId));
    }

    // Writes all chests linked to a shop to the database
    public static void writeShopChests(Shop shop) {
        writeShopChests(shop.getId(), shop.getInventory().getChests());
    }

    // Writes a collection of locations to the database
    public static void writeShopChests(int shopId, Collection<Location> chests) {
        if (chests.isEmpty()) {
            return;
        }
        
        StringBuilder query = new StringBuilder();
        for (Location loc : chests) {
            if (query.length() > 0) {
                query.append(",");
            }
            query.append("(")
                    .append(shopId)
                    .append(",")
                    .append("'").append(loc.getWorld().getName()).append("'")
                    .append(",")
                    .append(loc.getX())
                    .append(",")
                    .append(loc.getY())
                    .append(",")
                    .append(loc.getZ())
                    .append(")");
        }
        Queue.queueQuery(INSERT + query.toString());
    }

    // Reads shop chests from database and applies them to shop
    public static void loadShopChests(Shop shop) {
        shop.setInventory(new MultiChestInventory(readShopChests(shop.getId())));
    }

    // Returns shop chests from database
    public static List<Location> readShopChests(int shopId) {
        List<Location> chests = new ArrayList();
        try (Statement stmt = Database.inst().getStatement()) {
            ResultSet rs = stmt.executeQuery(String.format(SELECT_ALL, shopId));
            while (rs.next()) {
                World world = Bukkit.getWorld(rs.getString(1));
                if (world == null) {
                    continue;
                }

                int x = rs.getInt(2);
                int y = rs.getInt(3);
                int z = rs.getInt(4);

                chests.add(new Location(world, x, y, z));
            }
        } catch (SQLException e) {
            DarkMarket.LOG.log(Level.SEVERE, null, e);
        }

        return chests;
    }

    public static void removeShopsChests(int shopId, Collection<Location> chests) {
        if (chests.isEmpty()) {
            return;
        }
        
        StringBuilder query = new StringBuilder();
        for (Location loc : chests) {
            if (query.length() > 0) {
                query.append(",");
            }
            query.append("(")
                    .append(shopId).append(',')
                    .append("'").append(loc.getWorld().getName()).append("'")
                    .append(",")
                    .append(loc.getBlockX())
                    .append(",")
                    .append(loc.getBlockY())
                    .append(",")
                    .append(loc.getBlockZ())
                    .append(")");
        }
        Queue.queueQuery(String.format(DELETE_MULTIPLE, query.toString()));
    }
    
    public static void updateShopChests(Shop shop, Collection<Location> before, Collection<Location> after) {
        updateShopChests(shop.getId(), before, after);
    }

    // updates shop chests, adding and removing chests as necessary
    public static void updateShopChests(int shopId, Collection<Location> before, Collection<Location> after) {
        ArrayList<Location> toAdd = new ArrayList();
        ArrayList<Location> toRemove = new ArrayList();
        for (Location loc : before) {
            if (!after.contains(loc)) {
                toRemove.add(loc);
            }
        }
        for (Location loc : after) {
            if (!before.contains(loc)) {
                toAdd.add(loc);
            }
        }
        removeShopsChests(shopId, toRemove);
        writeShopChests(shopId, toAdd);
    }
    
    public static boolean isShopChest(Block chest) {
        if (isLinked(chest)) {
            return true;
        }
        
        Block partner = uBlock.getChestPair(chest);
        return partner != null && isLinked(partner);
    }
    
    private static boolean isLinked(Block chest) {
        try (Statement stmt = Database.inst().getStatement()) {
            ResultSet rs = stmt.executeQuery(String.format(SELECT_SHOP,
                    chest.getWorld().getName(),
                    chest.getX(),
                    chest.getY(),
                    chest.getZ()));
            
            return rs.next();
        } catch (SQLException e) {
            DarkMarket.LOG.log(Level.SEVERE, null, e);
        }
        return false;
    }
    
    // gets the shop id associated with this (double) chest
    public static List<Integer> getShopIdsByChest(Block chest) {
        List<Integer> ids = new ArrayList();
        
        ids.addAll(getShopIdsByBlock(chest));
            
        Block partner = uBlock.getChestPair(chest);
        if (partner != null) {
            ids.addAll(getShopIdsByBlock(partner));
        }

        return ids;
    }
    
    // gets the shop id associated with this block
    public static List<Integer> getShopIdsByBlock(Block chest) {
        List<Integer> ids = new ArrayList();
        try (Statement stmt = Database.inst().getStatement()) {
            ResultSet rs = stmt.executeQuery(String.format(SELECT_SHOP,
                    chest.getWorld().getName(),
                    chest.getX(),
                    chest.getY(),
                    chest.getZ()));
            
            while (rs.next()) {
                ids.add(rs.getInt(1));
            }
        } catch (SQLException e) {
            DarkMarket.LOG.log(Level.SEVERE, null, e);
        }
        return ids;
    }
    
}
