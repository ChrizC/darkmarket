package org.darkcraft.darkmarket.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import org.darkcraft.darkmarket.DarkMarket;
import org.darkcraft.darkmarket.event.ShopCreationEvent;
import org.darkcraft.darkmarket.shop.Shop;

public class Shops {
    
    private static final String INSERT_SHOP =
            "INSERT INTO `shops` (owner, admin, world, x, y, z, buy, sell, quantity, `type`, durability, meta_id)"
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SELECT_LOCATION =
            "SELECT world, x, y, z FROM `shops` WHERE id = %d";
    private static final String SELECT_SHOP =
            "SELECT id, owner, admin, buy, sell, quantity, `type`, durability, meta_id FROM `shops` WHERE"
            + " world = '%s' AND x = %d AND y = %d AND z = %d;";
    private static final String DELETE_SHOP_ID =
            "DELETE FROM `shops` WHERE id = %d";
    private static final String UPDATE_SHOP = "UPDATE `shops` SET owner = ?, buy = ?, sell = ?, quantity = ?, type = ?, durability = ?,"
            + " meta_id = ? WHERE id = ?";
    
    public static Shop getByLocation(Location loc) {
        try (Statement stmt = Database.inst().getStatement()) {
            ResultSet result = stmt.executeQuery(String.format(SELECT_SHOP, loc.getWorld().getName(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()));
            
            if (!result.next()) {
                return null;
            }
            
            int id = result.getInt(1);
            OfflinePlayer owner = Bukkit.getOfflinePlayer(result.getString(2));
            boolean admin = result.getBoolean(3);
            
            Block sign = loc.getBlock();
            
            double buy = result.getDouble(4);
            double sell = result.getDouble(5);
            
            int quantity = result.getInt(6);
            ItemStack item = new ItemStack(result.getInt(7), quantity, result.getShort(8));
            
            int metaId = result.getInt(9);
            if (metaId > -1) {
                item.setItemMeta(Items.getMetaById(metaId));
            }
            
            return new Shop(id, owner, admin, sign, item, quantity, buy, sell);
        } catch (SQLException ex) {
            DarkMarket.LOG.log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static Shop createShop(ShopCreationEvent event) {
        return createShop(event.getCreator(), event.isAdminShop(), event.getSign().getBlock(), event.getItem(), event.getQuantity(), event.getBuyPrice(), event.getSellPrice());
    }
    
    public static Shop createShop(OfflinePlayer owner, boolean admin, Block sign, ItemStack item, int quantity, double buy, double sell) {
        int id = -1;
        try (PreparedStatement pstmt = Database.inst().prepareStatement(INSERT_SHOP, true)) {
            pstmt.setString(1, owner.getName());
            
            pstmt.setBoolean(2, admin);
            
            pstmt.setString(3, sign.getWorld().getName());
            pstmt.setInt(4, sign.getX());
            pstmt.setInt(5, sign.getY());
            pstmt.setInt(6, sign.getZ());
            
            pstmt.setDouble(7, buy);
            pstmt.setDouble(8, sell);
            
            pstmt.setInt(9, quantity);
            pstmt.setInt(10, item.getTypeId());
            pstmt.setDouble(11, item.getDurability());
            pstmt.setInt(12, item.hasItemMeta()? Items.getMetaId(item.getItemMeta()) : -1);
            
            pstmt.execute();
            
            ResultSet result = pstmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            DarkMarket.LOG.log(Level.SEVERE, null, ex);
            return null;
        }
        
        if (id < 0) {
            return null;
        }
        
        return new Shop(id, owner, admin, sign, item, quantity, buy, sell);
    }
    
    public static void updateShop(Shop shop) {
        if (shop.getId() < 0) {
            return;
        }
        
        Queue.queue(new ShopUpdateRow(shop.getId(), shop.getOwner().getName(), shop.getBuyPrice(), shop.getSellPrice(), shop.getQuantity(), shop.getItem()));
    }
    
    private static class ShopUpdateRow implements Queue.PreparedRow {
        
        int id;
        String owner;
        double buy, sell;
        int quantity;
        ItemStack item;
        
        public ShopUpdateRow(int id, String owner, double buy, double sell, int quantity, ItemStack item) {
            this.id = id;
            this.owner = owner;
            this.buy = buy;
            this.sell = sell;
            this.quantity = quantity;
            this.item = item;
        }

        @Override
        public void executeStatement(Connection conn) throws SQLException {
            try (PreparedStatement pstmt = conn.prepareStatement(UPDATE_SHOP)) {
                pstmt.setInt(8, id);

                pstmt.setString(1, owner);
                pstmt.setDouble(2, buy);
                pstmt.setDouble(3, sell);
                pstmt.setInt(4, quantity);
                pstmt.setInt(5, item.getTypeId());
                pstmt.setInt(6, item.getDurability());
                pstmt.setInt(7, item.hasItemMeta()? Items.getMetaId(item.getItemMeta()) : -1);

                pstmt.execute();
            }
        }

        @Override
        public String[] getInserts() {
            return null;
        }
        
    }
    
    public static void deleteShop(Shop shop) {
        if (shop.getId() < 0) {
            return;
        }
        
        deleteShop(shop.getId());
    }
    
    public static void deleteShop(int id) {
        Queue.queueQuery(String.format(DELETE_SHOP_ID, id));
    }
    
    public static Location getShopLocation(int id) {
        try (Statement stmt = Database.inst().getStatement()) {
            ResultSet rs = stmt.executeQuery(String.format(SELECT_LOCATION, id));
            
            if (!rs.next()) {
                return null;
            }
            
            World world = Bukkit.getWorld(rs.getString(1));
            if (world == null) {
                return null;
            }
            
            return new Location(world, rs.getInt(2), rs.getInt(3), rs.getInt(4));
        } catch (SQLException ex) {
            DarkMarket.LOG.log(Level.SEVERE, null, ex);
        }
        return null;
    }    
    
}
