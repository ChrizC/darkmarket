package org.darkcraft.darkmarket.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import org.darkcraft.darkmarket.DarkMarket;

public class Queue extends TimerTask {
    
    private static final int ROWS_PER_RUN = 100;
    private static final int TIME_PER_RUN = 1000;
    
    private static LinkedBlockingQueue<Row> queue = new LinkedBlockingQueue();
    private static Lock lock = new ReentrantLock();

    @Override
    public void run() {
        if (queue.isEmpty() || !lock.tryLock()) {
            return;
        }
        
        if (queue.size() > ROWS_PER_RUN) {
            DarkMarket.LOG.warning("SQL Queue overloaded: " + queue.size());
        }
        
        try (Connection connection = Database.getNewConnection()) {
            connection.setAutoCommit(false);
            
            try (Statement statement = connection.createStatement()) {
                
                int count = 0;
                long start = System.currentTimeMillis();
                
                while (!queue.isEmpty() && count < ROWS_PER_RUN && System.currentTimeMillis() - start < TIME_PER_RUN) {
                    Row row = queue.poll();
                    
                    if (row instanceof PreparedRow) {
                        ((PreparedRow) row).executeStatement(connection);
                    } else {
                        for (String string : row.getInserts()) {
                            statement.execute(string);
                        }
                    }
                    
                    count++;
                }
            }
            
            connection.commit();
            lock.unlock();
        } catch (SQLException e) {
            DarkMarket.LOG.log(Level.SEVERE, "Exception while executing query:", e);
        }
    }
    
    public interface Row {
        
        public String[] getInserts();
        
    }
    
    public interface PreparedRow extends Row {
        
        public void executeStatement(Connection conn) throws SQLException;
        
    }
    
    private static class SimpleQuery implements Row {
        
        private String[] inserts;
        
        public SimpleQuery(String... inserts) {
            this.inserts = inserts;
        }

        @Override
        public String[] getInserts() {
            return inserts;
        }
        
    }
    
    public static void queueQuery(String... queries) {
        queue.add(new SimpleQuery(queries));
    }
    
    public static void queue(Row row) {
        queue.add(row);
    }
    
    public static boolean isEmpty() {
        return queue.isEmpty();
    }

}
