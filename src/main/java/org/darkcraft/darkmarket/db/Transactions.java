package org.darkcraft.darkmarket.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.darkcraft.darkmarket.event.TransactionEvent;
import org.darkcraft.darkmarket.event.TransactionEvent.TransactionType;
import org.darkcraft.darkmarket.utils.ItemUtil;

public class Transactions {
    
    public static final String LOG_QUERY = "INSERT INTO transactions (shop, client, buy, price, quantity, type, data, enchantments)"
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    
    public static void logTransaction(TransactionEvent event) {
        Queue.queue(new TransactionRow(event));
    }
    
    private static class TransactionRow implements Queue.PreparedRow {
        
        private TransactionEvent event;
        
        public TransactionRow(TransactionEvent event) {
            this.event = event;
        }

        @Override
        public void executeStatement(Connection conn) throws SQLException {
            try (PreparedStatement pstmt = conn.prepareStatement(LOG_QUERY)) {
                pstmt.setString(1, event.getShop().getOwnerName());
                pstmt.setString(2, event.getClient().getName());
                pstmt.setBoolean(3, event.getType() == TransactionType.BUY);
                pstmt.setDouble(4, event.getPrice());
                pstmt.setInt(5, event.getQuantity());
                pstmt.setInt(6, event.getItem().getTypeId());
                pstmt.setShort(7, event.getItem().getDurability());
                pstmt.setString(8, ItemUtil.enchantmentsToString(event.getItem().getEnchantments()));

                pstmt.execute();
            }
        }

        @Override
        public String[] getInserts() {
            return null;
        }
        
    }

}
