package org.darkcraft.darkmarket.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.inventory.meta.ItemMeta;
import org.darkcraft.darkmarket.DarkMarket;
import org.darkcraft.darkmarket.utils.ConfigurationByteSerialization;

public class Items {
    
    private static final String INSERT = "INSERT INTO meta (bytes) VALUES (?)";
    private static final String SELECT = "SELECT bytes FROM meta WHERE id = %d";
    
    private static final Map<Integer, ItemMeta> META_CACHE = new HashMap();
    
    public static int getMetaId(ItemMeta meta) {
        for (Map.Entry<Integer, ItemMeta> entry : META_CACHE.entrySet()) { // look for meta already in cache
            if (Bukkit.getItemFactory().equals(meta, entry.getValue())) {
                return entry.getKey();
            }
        }
        
        try (PreparedStatement pstmt = Database.inst().prepareStatement(INSERT, true)) {
            pstmt.setBytes(1, ConfigurationByteSerialization.serializeToBytes(meta));
            
            pstmt.execute();
            
            try (ResultSet rs = pstmt.getGeneratedKeys()) {
                if (!rs.next()) {
                    return -1;
                }
                int id = rs.getInt(1);
                
                META_CACHE.put(id, meta);
                
                return id;
            }
        } catch (SQLException e) {
            DarkMarket.LOG.log(Level.SEVERE, null, e);
        }
        return -1;
    }
    
    public static ItemMeta getMetaById(int id) {
        if (META_CACHE.containsKey(id)) {
            return META_CACHE.get(id);
        }
        
        try (Statement statement = Database.inst().getStatement()) {
            ResultSet results = statement.executeQuery(String.format(SELECT, id));
            
            if (!results.next()) {
                return null;
            }
            
            byte[] byes = results.getBytes(1);
            ItemMeta meta = (ItemMeta) ConfigurationByteSerialization.deserializeFromBytes(byes);
            
            return meta;
        } catch (SQLException e) {
            DarkMarket.LOG.log(Level.SEVERE, null, e);
        }
        return null;
    }

}
