package org.darkcraft.darkmarket;

import com.daemitus.deadbolt.Deadbolt;
import java.io.File;
import java.sql.SQLException;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.darkcraft.darkmarket.db.Database;
import org.darkcraft.darkmarket.db.Queue;
import org.darkcraft.darkmarket.listener.block.Protection;
import org.darkcraft.darkmarket.listener.shop.ShopEdit;
import org.darkcraft.darkmarket.listener.block.SignChange;
import org.darkcraft.darkmarket.listener.inventory.BulkManager;
import org.darkcraft.darkmarket.listener.player.ChestLink;
import org.darkcraft.darkmarket.listener.player.PlayerInteract;
import org.darkcraft.darkmarket.listener.shop.DirectInput;
import org.darkcraft.darkmarket.listener.shop.PreShopCreation;
import org.darkcraft.darkmarket.listener.shop.ShopCreation;
import org.darkcraft.darkmarket.listener.shop.ShopDestruction;
import org.darkcraft.darkmarket.listener.transaction.PreTransactionConfirmer;
import org.darkcraft.darkmarket.listener.transaction.PreTransaction;
import org.darkcraft.darkmarket.listener.transaction.PreTransactionSpam;
import org.darkcraft.darkmarket.listener.transaction.Transaction;
import org.darkcraft.darkmarket.listener.transaction.TransactionLogger;
import org.darkcraft.darkmarket.listener.transaction.TransactionMessenger;
import org.yi.acru.bukkit.Lockette.Lockette;

public class DarkMarket extends JavaPlugin {
    
    private static DarkMarket instance; // Singleton
    public static Logger LOG; // Log
    
    private Timer timer;
    private Queue queue;
    
    private static boolean hasLockette = false;
    private static boolean hasDeadbolt = false;
    
    public static DarkMarket getInstance() {
        return instance;
    }

    public DarkMarket() {
        instance = this;
    }
    
    @Override
    public void onLoad() {
        Config.init(new File(getDataFolder(), "/config.yml"));
        
        try {
            Database.inst().open();
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, "SQL Connection Failed", e);
            return;
        }
        
        queue = new Queue();
        timer = new Timer();
        timer.scheduleAtFixedRate(queue, Config.timeBetweenQueueRun * 1000, Config.timeBetweenQueueRun * 1000);
    }
    
    @Override
    public void onEnable() {
        this.LOG = getLogger();
        
        registerEvents();
        checkDependencies();
    }
    
    @Override
    public void onDisable() {
        try {
            Database.inst().close();
        } catch (SQLException e) {
            getLogger().log(Level.WARNING, "Exception closing MySQL connection:", e);
        }
        
        if (timer != null) {
            timer.cancel();
        }
        
        while (!Queue.isEmpty()) {
            LOG.info("Waiting for queue..");
            queue.run();
        }
    }
    
    private void registerEvents() {
        register(new SignChange());
        register(new PlayerInteract());
        register(new Protection());
        
        register(new PreTransaction());
        register(new PreTransactionSpam());
        register(new PreTransactionConfirmer());
        
        register(new Transaction());
        register(new TransactionMessenger());
        register(new TransactionLogger());
        
        register(new PreShopCreation());
        register(new DirectInput());
        
        register(new ShopCreation());
        register(new ShopDestruction());
        
        register(new ChestLink());
        register(new ShopEdit());
        
        register(new BulkManager());
    }
    
    private void register(Listener listener) {
        Bukkit.getPluginManager().registerEvents(listener, this);
    }
    
    public void checkDependencies() {
        hasLockette = Bukkit.getPluginManager().isPluginEnabled("Lockette");
        hasDeadbolt = Bukkit.getPluginManager().isPluginEnabled("Deadbolt");
    }
    
    public static boolean isLockedChestOwner(OfflinePlayer player, Block block) {
        if (hasLockette) {
            return !Lockette.isProtected(block) || Lockette.isOwner(block, player.getName());
        }
        if (hasDeadbolt) {
            return !Deadbolt.isProtected(block) || Deadbolt.getOwnerName(block).equals(player.getName());
        }
        return true;
    }
    
}
