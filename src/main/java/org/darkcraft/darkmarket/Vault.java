package org.darkcraft.darkmarket;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

public class Vault {
    
    private static Economy econ;
    private static Chat chat;
    private static Permission perm;
    
    public static Economy getEconomy() {
        if (econ == null) {
            RegisteredServiceProvider<Economy> rsp = Bukkit.getServicesManager().getRegistration(Economy.class);
            if (rsp != null)
                econ = rsp.getProvider();
        }
        return econ;
    }
    
    public static Chat getChat() {
        if (chat == null) {
            RegisteredServiceProvider<Chat> rsp = Bukkit.getServicesManager().getRegistration(Chat.class);
            if (rsp != null)
                chat = rsp.getProvider();
        }
        return chat;
    }
    
    public static Permission getPermission() {
        if (perm == null) {
            RegisteredServiceProvider<Permission> rsp = Bukkit.getServicesManager().getRegistration(Permission.class);
            if (rsp != null)
                perm = rsp.getProvider();
        }
        return perm;
    }
    
}
