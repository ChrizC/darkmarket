package org.darkcraft.darkmarket.event;

import java.util.List;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.darkcraft.darkmarket.event.PreShopCreationEvent.Input;

public class DirectInputEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    
    private final PreShopCreationEvent event;
    
    private Input current = null;
    
    public DirectInputEvent(PreShopCreationEvent event) {
        this.event = event;
    }
    
    public PreShopCreationEvent getPreShopCreationEvent() {
        return event;
    }    
    
    public Input getCurrentInput() {
        return current;
    }
    
    public void setCurrentInput(Input input) {
        this.current = input;
    }    
    
    public List<Input> getRemainingInput() {
        return event.getRemainingInput();
    }
    
    public void completeInput(Input input) {
        event.removeInput(input);
    }
    
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {   
        return handlers;
    }

}
