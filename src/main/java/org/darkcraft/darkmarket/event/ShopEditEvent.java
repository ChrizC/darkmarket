package org.darkcraft.darkmarket.event;

import org.bukkit.OfflinePlayer;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.darkcraft.darkmarket.Message;
import org.darkcraft.darkmarket.db.Shops;
import org.darkcraft.darkmarket.listener.block.SignChange;
import org.darkcraft.darkmarket.shop.Shop;
import org.darkcraft.darkmarket.shop.ShopSign;

import static org.darkcraft.darkmarket.listener.player.ChestLink.registerLinker;
import static org.darkcraft.darkmarket.utils.uBlock.destroyLater;

public class ShopEditEvent extends PreShopCreationEvent {
    
    private Shop shop;
    
    private OfflinePlayer owner;
    
    private boolean editingChests = false;
    
    public ShopEditEvent(Player player, Shop shop, Sign placed, String[] lines) {
        super(player, placed, lines);
        this.shop = shop;
        this.owner = shop.getOwner();
        
        this.setItem(shop.getItem());
        this.setQuantity(shop.getQuantity());
        
        this.setBuyPrice(shop.getBuyPrice());
        this.setSellPrice(shop.getSellPrice());
    }
    
    public Shop getShop() {
        return shop;
    }
    
    public OfflinePlayer getOwner() {
        return owner;
    }
    
    public void setOwner(OfflinePlayer player) {
        this.owner = player;
    }
    
    public boolean isEditingChests() {
        return editingChests;
    }
    
    public void setEditingChests(boolean editingChests) {
        this.editingChests = editingChests;
    }
    
    public void saveEdits() {
        shop.setOwner(owner);
        
        shop.setItem(getItem());
        shop.setQuantity(getQuantity());
        
        shop.setBuyPrice(getBuyPrice());
        shop.setSellPrice(getSellPrice());
        
        Shops.updateShop(shop);
        ShopSign.formatShopSign(shop, false);
        destroyLater(getSign().getBlock(), 1L);
        
        Message.INFO.send(getCreator(), COMPLETION);
        
        if (editingChests) {
            registerLinker(getCreator(), shop);
        }
        
        SignChange.removeEditor(getCreator());
    }
    
    private static final String COMPLETION = "Successfully edited shop!";
    
}
