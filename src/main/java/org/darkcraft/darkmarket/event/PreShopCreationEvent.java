package org.darkcraft.darkmarket.event;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.darkcraft.darkmarket.utils.ItemStackUtil;

public class PreShopCreationEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    
    private Player creator;
    
    private Sign sign;
    private String[] lines;
    
    private ItemStack item = null;
    private int quantity = 0;
    
    private double buyPrice = -1;
    private double sellPrice = -1;
    
    private boolean admin = false;
    
    private State state = State.SHOP_CREATED_SUCCESSFULLY;
    private List<Input> remainingInput = new ArrayList(Arrays.asList(Input.values()));
    
    public PreShopCreationEvent(Player creator, Sign sign, String[] lines) {
        this.creator = creator;
        this.sign = sign;
        this.lines = lines;
    }
    
    public boolean isCancelled() {
        return state != State.SHOP_CREATED_SUCCESSFULLY;
    }

    public State getState() {
        return state;
    }

    public void setState(State outcome) {
        this.state = outcome;
    }

    public void setCreator(Player creator) {
        this.creator = creator;
    }

    public void setSign(Sign sign) {
        this.sign = sign;
    }
    
    public Player getCreator() {
        return creator;
    }

    public Sign getSign() {
        return sign;
    }

    public String getSignLine(int line) {
        return lines[line];
    }

    public String[] getSignLines() {
        return lines;
    }
    
    public boolean hasLine(int line) {
        return lines[line] != null && !lines[line].trim().isEmpty();
    }
    
    public ItemStack getItem() {
        return item;
    }
    
    public void setItem(ItemStack item) {
        this.item = ItemStackUtil.clone(item);
        
        if (ItemStackUtil.isTool(item.getTypeId())) {
            this.item.setDurability((short) 0);
        }
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public void setQuantity(int amount) {
        this.quantity = amount;
    }
    
    public double getBuyPrice() {
        return buyPrice;
    }
    
    public double getSellPrice() {
        return sellPrice;
    }
    
    public void setBuyPrice(double price) {
        this.buyPrice = price;
    }
    
    public boolean isAdminShop() {
        return admin;
    }
    
    public void setAdminShop(boolean admin) {
        this.admin = admin;
    }
    
    public boolean requiresInput() {
        return remainingInput.size() > 0;
    }
    
    public List<Input> getRemainingInput() {
        return remainingInput;
    }
    
    public void removeInput(Input input) {
        remainingInput.remove(input);
    }
    
    public void setSellPrice(double price) {
        this.sellPrice = price;
    }
    
    public boolean isShopEditEvent() {
        return this instanceof ShopEditEvent;
    }
    
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {   
        return handlers;
    }
    
    public static enum State {
        INVALID_ITEM,
        INVALID_PRICE,
        INVALID_QUANTITY,
        
        NO_PERMISSION,
        SELL_PRICE_HIGHER_THAN_BUY_PRICE,

        SHOP_CREATED_SUCCESSFULLY
    }
    
    public static enum Input {
        ITEM,
        QUANTITY,
        PRICE
    }
    
}
