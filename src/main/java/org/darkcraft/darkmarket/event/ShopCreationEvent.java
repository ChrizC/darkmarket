package org.darkcraft.darkmarket.event;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class ShopCreationEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    
    private final Player creator;
    
    private final Sign sign;
    
    private final ItemStack item;
    private final int quantity;
    
    private final double buyPrice;
    private final double sellPrice;
    
    private final boolean admin;
    
    public ShopCreationEvent(PreShopCreationEvent event) {
        this(event.getCreator(), event.getSign(), event.getItem(), event.getQuantity(), event.getBuyPrice(), event.getSellPrice(), event.isAdminShop());
    }
    
    public ShopCreationEvent(Player creator, Sign sign, ItemStack item, int quantity, double buyPrice, double sellPrice, boolean admin) {
        this.creator = creator;
        this.sign = sign;
        this.item = item;
        this.quantity = quantity;
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
        this.admin = admin;
    }
    
    public Player getCreator() {
        return creator;
    }
    
    public Sign getSign() {
        return sign;
    }
    
    public ItemStack getItem() {
        return item;
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public double getBuyPrice() {
        return buyPrice;
    }
    
    public double getSellPrice() {
        return sellPrice;
    }
    
    public boolean isAdminShop() {
        return admin;
    }

    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {   
        return handlers;
    }
    
}
