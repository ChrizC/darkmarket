package org.darkcraft.darkmarket.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.darkcraft.darkmarket.shop.Shop;

public class TransactionEvent extends Event {
    public static final HandlerList handlers = new HandlerList();
    
    private final Shop shop;
    private final Player client;
    
    private final ItemStack item;
    private final int quantity;
    
    private final double price;
    
    private final TransactionType type;
    
    public TransactionEvent(PreTransactionEvent event) {
        this(event.getShop(), event.getClient(), event.getItem(), event.getQuantity(), event.getPrice(), event.getType());
    }
    
    public TransactionEvent(Shop shop, Player client, ItemStack item, int quantity, double price, TransactionType type) {
        this.shop = shop;
        this.client = client;
        this.item = item;
        this.quantity = quantity;
        this.price = price;
        this.type = type;
    }
    
    public Shop getShop() {
        return shop;
    }
    
    public Player getClient() {
        return client;
    }
    
    public ItemStack getItem() {
        return item;
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public double getPrice() {
        return price;
    }
    
    public TransactionType getType() {
        return type;
    }
    
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {   
        return handlers;
    }    
                
    public static enum TransactionType {
        BUY, SELL
    }
    
}
