package org.darkcraft.darkmarket.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.darkcraft.darkmarket.shop.Shop;

public class ShopDestructionEvent extends Event implements Cancellable {
    public static HandlerList handlers = new HandlerList();
    
    private final Player player;
    
    private final Shop shop;
    
    private boolean cancelled;
    
    public ShopDestructionEvent(Shop shop) {
        this(null, shop);
    }
    
    public ShopDestructionEvent(Player player, Shop shop) {
        this.player = player;
        this.shop = shop;
    }
    
    public boolean isPlayerAction() {
        return player != null;
    }
    
    public Player getPlayer() {
        return player;
    }
    
    public Shop getShop() {
        return shop;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean bln) {
        this.cancelled = true;
    }
    
}
