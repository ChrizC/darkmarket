package org.darkcraft.darkmarket.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.darkcraft.darkmarket.shop.Shop;

import static org.darkcraft.darkmarket.event.TransactionEvent.TransactionType;

public class PreTransactionEvent extends TransactionEvent implements Cancellable {
    public static final HandlerList handlers = new HandlerList();
    
    private State outcome = State.TRANSACTION_SUCCESSFUL;
    
    public PreTransactionEvent(Shop shop, Player client, ItemStack item, int quantity, double price, TransactionType type) {
        super(shop, client, item, quantity, price, type);
    }

    public State getState() {
        return outcome;
    }
    
    public void setState(State outcome) {
        this.outcome = outcome;
    }
    
    @Override
    public boolean isCancelled() {
        return outcome != State.TRANSACTION_SUCCESSFUL;
    }
    
    @Override
    public void setCancelled(boolean cancel) {}
    
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    public enum State {
        NO_PERMISSION,
        
        CONFIRMATION_REQUIRED,
        SPAM_PREVENTION,
        
        SHOP_HAS_NO_INVENTORY,
        
        SHOP_DOES_NOT_BUY_THIS_ITEM,
        SHOP_DOES_NOT_SELL_THIS_ITEM,

        CLIENT_DOES_NOT_HAVE_ENOUGH_MONEY,
        SHOP_DOES_NOT_HAVE_ENOUGH_MONEY,

        NOT_ENOUGH_SPACE_IN_SHOP,
        NOT_ENOUGH_SPACE_IN_INVENTORY,

        NOT_ENOUGH_STOCK_IN_SHOP,
        NOT_ENOUGH_STOCK_IN_INVENTORY,

        TRANSACTION_SUCCESSFUL
    }
    
}
