package org.darkcraft.darkmarket.utils;

import java.util.Map;
import net.milkbowl.vault.item.ItemInfo;
import net.milkbowl.vault.item.Items;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class ItemUtil {

    public static String getItemInfoSignName(ItemInfo info) {
        String line = info.getName();
        
        if (line.length() > 15) {
            line = line.replaceAll(" ", "");
        }
        
        if (line.length() > 15) {
            StringBuilder sb = new StringBuilder();
            for (String s : info.search[0]) {
                sb.append(Character.toUpperCase(s.charAt(0)));
                sb.append(s.substring(1));
            }
            line = sb.toString();
        }
        
        return line;
    }
    
    public static String getItemName(ItemStack item) {
        ItemInfo info = Items.itemByStack(item);
        return info != null? info.getName() : StringUtil.capitalizeFirst(item.getType().toString());
    }
    
    public static String getItemSignName(ItemStack item) {
        ItemInfo info = Items.itemByStack(item);
        if (info == null) {
            String name = StringUtil.capitalizeFirst(item.getType().toString());
            return name.length() > 15? name.substring(0, 15) : name;
        } else {
            return getItemInfoSignName(Items.itemByStack(item));
        }
    }
    
    public static String enchantmentsToString(Map<Enchantment, Integer> enchs) {
        if (enchs == null || enchs.isEmpty()) {
            return null;
        }
        
        StringBuilder sb = new StringBuilder();
        
        for (Enchantment ench : enchs.keySet()) {
            if (sb.length() > 0) {
                sb.append(',');
            }
            sb.append(ench.getName()).append('x').append(enchs.get(ench));
        }
        
        return sb.toString();
    }

}
