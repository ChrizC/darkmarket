package org.darkcraft.darkmarket.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.enchantments.Enchantment;

@Deprecated
public class uEnchant {
    
    public static Map<Enchantment, Integer> deserialize(String s) {
        if (s == null || s.isEmpty()) {
            return Collections.emptyMap();
        }
        
        HashMap<Enchantment, Integer> enchantments = new HashMap();
        for (String sect : s.split(",")) {
            String[] parts = sect.split("x");
            enchantments.put(Enchantment.getByName(parts[0]), Integer.parseInt(parts[1]));
        }
        return enchantments;
    }
    
}