package org.darkcraft.darkmarket.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

public class StringUtil {

    public static String join(String[] array, char glue) {
        return join(array, glue, 0);
    }

    public static String join(String[] array, char glue, int start) {
        return StringUtils.join(array, glue, start, array.length);
    }

    /**
     * Capitalize the first letter after every space in a string
     *
     * @param s the string format
     * @param delimiter the delimeter already in place, to be made spaces
     * @return formatted string
     */
    public static String capitalizeFirst(String s, char delimiter) {
        return WordUtils.capitalizeFully(s.replace(delimiter, ' '));
    }

    public static String capitalizeFirst(String s) {
        return capitalizeFirst(s, '_');
    }
}
