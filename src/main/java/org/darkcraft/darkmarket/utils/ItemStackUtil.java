package org.darkcraft.darkmarket.utils;

import java.util.HashSet;
import java.util.Set;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.Repairable;
import org.darkcraft.darkmarket.Config;

public class ItemStackUtil {
    
    public static boolean equals(ItemStack one, ItemStack two) {
        if (one == two) {
            return true;
        }
        
        if (one == null || two == null) {
            return false;
        }
        
        if (one.getTypeId() != two.getTypeId() || one.getDurability() != two.getDurability()) {
            return false;
        }
        
        if (Config.ignoreRepairCost) {
            one = removeRepairCost(one);
            two = removeRepairCost(two);
        }
        
        return one.hasItemMeta()? two.hasItemMeta() && Bukkit.getItemFactory().equals(one.getItemMeta(), two.getItemMeta()) : !two.hasItemMeta();
    }
    
    public static boolean isEmpty(ItemStack item) {
        return item == null || item.getAmount() == 0 || item.getTypeId() == 0;
    }
    
    /**
     * Removes the repair cost from an item. If the given Repairable does
     * not have a set repair cost, a reference to the given ItemStack is returned
     */
    private static ItemStack removeRepairCost(ItemStack item) {
        if (!item.hasItemMeta()) {
            return item;
        }
        
        Repairable meta = (Repairable) item.getItemMeta();
        if (!meta.hasRepairCost()) {
            return item;
        }
        
        item = new ItemStack(item);
        meta.setRepairCost(0);
        item.setItemMeta((ItemMeta) meta);
        return item;
    }
    
    public static ItemStack clone(ItemStack item) {
        return clone(item, item.getAmount());
    }
    
    public static ItemStack clone(ItemStack item, int amount) {
        ItemStack cloned = new ItemStack(item);
        
        if (Config.ignoreRepairCost) {
            cloned = removeRepairCost(cloned);
        }
        
        cloned.setAmount(amount);
        
        return cloned;
    }
    
    private static final Set<Integer> toolIds = new HashSet();
    
    public static boolean isTool(int id) {
        return toolIds.contains(id);
    }
    
    static {
        //wood
        toolIds.add(268);
        toolIds.add(269);
        toolIds.add(270);
        toolIds.add(271);
        toolIds.add(290);
        //stone
        toolIds.add(272);
        toolIds.add(273);
        toolIds.add(274);
        toolIds.add(275);
        toolIds.add(291);
        //iron
        toolIds.add(256);
        toolIds.add(257);
        toolIds.add(258);
        toolIds.add(267);
        toolIds.add(292);
        //gold
        toolIds.add(283);
        toolIds.add(284);
        toolIds.add(285);
        toolIds.add(286);
        toolIds.add(294);
        //diamond
        toolIds.add(276);
        toolIds.add(277);
        toolIds.add(278);
        toolIds.add(279);
        toolIds.add(293);
        
        //misc
        toolIds.add(259); // flint and steel
        toolIds.add(261); // bow
        toolIds.add(346); // fishing rod
        toolIds.add(398); // carrot on a stick
        toolIds.add(359); // shears
        
        //armour
        for (int id = 298; id < 318; id++) {
            toolIds.add(id);
        }
    }
    
}
