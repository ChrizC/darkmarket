package org.darkcraft.darkmarket.utils;

import java.util.List;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.darkcraft.darkmarket.DarkMarket;

public class InventoryUtil {

    /**
     * Adds the specified amount of an item to an inventory.
     *
     * @return the amount of items that could not be added
     */
    public static int addItem(Inventory inventory, ItemStack item, int amount) {
        int size = inventory.getSize();
        final int max = item.getMaxStackSize();

        for (int i = 0; i < size && amount > 0; i++) {
            ItemStack is = inventory.getItem(i);
            if (ItemStackUtil.isEmpty(is)) {
                ItemStack clone = item.clone();
                if (amount >= max) {
                    clone.setAmount(max);
                    inventory.setItem(i, clone);
                    amount -= max;
                } else {
                    clone.setAmount(amount);
                    inventory.setItem(i, clone);
                    return 0;
                }
            } else {
                if (is.getAmount() >= max || !ItemStackUtil.equals(item, is)) {
                    continue;
                }

                int toFillStack = max - is.getAmount();
                if (amount >= toFillStack) {
                    is.setAmount(max);
                    amount -= toFillStack;
                } else {
                    is.setAmount(is.getAmount() + amount);
                    return 0;
                }
            }
        }
        return amount;
    }

    public static int removeItem(Inventory inventory, ItemStack item, int amount) {
        int size = inventory.getSize();

        for (int i = 0; i < size && amount > 0; i++) {
            final ItemStack is = inventory.getItem(i);

            if (ItemStackUtil.isEmpty(is) || !ItemStackUtil.equals(item, is)) {
                continue;
            }

            int left = is.getAmount();
            if (amount >= left) {
                amount -= left;
                inventory.clear(i);
            } else {
                is.setAmount(is.getAmount() - amount);
                amount = 0;
            }
        }

        return amount;
    }

    public static boolean contains(ItemStack[] contents, ItemStack item, int amnt) {
        for (ItemStack is : contents) {
            if (ItemStackUtil.equals(item, is) && (amnt -= is.getAmount()) <= 0) {
                return true;
            }
        }
        return false;
    }

    public static boolean fits(ItemStack[] contents, ItemStack item, int amnt) {
        for (ItemStack is : contents) {
            if (ItemStackUtil.isEmpty(is)) {
                if ((amnt -= item.getMaxStackSize()) <= 0) {
                    return true;
                }
                continue;
            }

            if (ItemStackUtil.equals(item, is) && (amnt -= is.getMaxStackSize() - is.getAmount()) <= 0) {
                return true;
            }
        }
        return false;
    }

    /*
     * 
     * Multiple inventory util
     * 
     */
    public static int has(List<Inventory> chests, ItemStack item) {
        int has = 0;
        for (Inventory inv : chests) {
            for (ItemStack is : inv.getContents()) {
                if (!ItemStackUtil.isEmpty(is) && ItemStackUtil.equals(item, is)) {
                    has += is.getAmount();
                }
            }
        }
        return has;
    }

    public static int space(List<Inventory> chests, ItemStack item) {
        int space = 0;
        final int stack = item.getMaxStackSize();

        for (Inventory inv : chests) {
            for (ItemStack is : inv.getContents()) {
                if (ItemStackUtil.isEmpty(is)) {
                    space += stack;
                } else if (ItemStackUtil.equals(item, is)) {
                    space += stack - is.getAmount();
                }
            }
        }
        return space;
    }

    public static int addItem(List<Inventory> chests, ItemStack item, int amount) {
        for (Inventory inventory : chests) {
            if (amount <= 0) {
                break;
            }

            amount = addItem(inventory, item, amount);
        }
        return amount;
    }

    public static int removeItem(List<Inventory> chests, ItemStack item, int amount) {
        for (Inventory inv : chests) {
            if (amount <= 0) {
                break;
            }

            amount = removeItem(inv, item, amount);
        }
        return amount;
    }
    
    /**
     * Updates a player's inventory 1 tick into the future
     */
    public static void updateInventoryLater(final Player player) {
        new BukkitRunnable() {

            @Override
            public void run() {
                player.updateInventory();
            }
        
        }.runTaskLater(DarkMarket.getInstance(), 1L);
    }
    
}
