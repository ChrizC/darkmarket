package org.darkcraft.darkmarket.utils;

import com.google.common.collect.ImmutableMap;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.material.Attachable;
import org.bukkit.scheduler.BukkitRunnable;
import org.darkcraft.darkmarket.DarkMarket;

public class uBlock {
    
    public static final BlockFace[] NWSE = new BlockFace[] {BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST};
    public static final BlockFace[] NWSEU = new BlockFace[] {BlockFace.UP, BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST};
    
    public static Block getChestPair(Block half) {
        for (BlockFace bf : NWSE) {
            Block relative = half.getRelative(bf);
            if (isDoubleChest(half, relative)) {
                return relative;
            }
        }
        return null;
    }
    
    public static boolean isSign(Block b) {
        return (b.getType() == Material.SIGN_POST || b.getType() == Material.WALL_SIGN || b.getState() instanceof Sign);
    }
    
    public static boolean isDoubleChest(Block a, Block b) {
        return (a.getType() == Material.CHEST && b.getType() == Material.CHEST)
                || (a.getType() == Material.TRAPPED_CHEST && b.getType() == Material.TRAPPED_CHEST);
    }
    
    public static boolean isChest(Block b) {
        return (b.getType() == Material.CHEST || b.getType() == Material.TRAPPED_CHEST || b.getState() instanceof Chest);
    }
    
    public static void setSign(Block block) {
        if (isSign(block)) {
            return;
        }
        
        if (!block.getRelative(BlockFace.DOWN).getType().isSolid()) {
            for (BlockFace bf : NWSE) {
                Block relative = block.getRelative(bf);
                if (!relative.getType().isSolid()) {
                    continue;
                }
                
                org.bukkit.material.Sign sign = new org.bukkit.material.Sign(Material.WALL_SIGN);
                sign.setFacingDirection(bf.getOppositeFace());

                block.setTypeIdAndData(Material.WALL_SIGN.getId(), sign.getData(), true);
                return;
            }
        }
        
        block.setType(Material.SIGN_POST);
    }
    
    public static List<Sign> getAttachedSigns(Block block) {
        List<Sign> signs = new ArrayList();
        for (BlockFace bf : NWSEU) {
            Block relative = block.getRelative(bf);
            
            if (!isSign(relative)) {
                continue;
            }
            
            Sign sign = (Sign) relative.getState();
            if (!block.equals(getSignAttachedBlock(sign))) {
                continue;
            }
            
            signs.add(sign);
        }
        return signs;
    }
    
    public static Block getSignAttachedBlock(Sign sign) {
        return sign.getBlock().getRelative(((Attachable) sign.getData()).getAttachedFace());
    }
    
    public static void destroyLater(final Block block, long delay) {
        new BukkitRunnable() {

            @Override
            public void run() {
                block.breakNaturally();
            }
            
        }.runTaskLater(DarkMarket.getInstance(), delay);
    }
    
}
