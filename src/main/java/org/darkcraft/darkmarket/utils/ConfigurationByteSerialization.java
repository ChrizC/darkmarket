package org.darkcraft.darkmarket.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.darkcraft.darkmarket.DarkMarket;

public class ConfigurationByteSerialization {
    
    public static byte[] serializeToBytes(ConfigurationSerializable object) {
        if (object == null) {
            return new byte[0];
        }
        
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos)) {                
            oos.writeObject(serialize(object));
            return baos.toByteArray();
        } catch (IOException ex) {
            DarkMarket.LOG.log(Level.SEVERE, "Failed to serialize ItemMeta", ex);
        }
        return new byte[0];
    }
    
    private static Map<String, Object> serialize(ConfigurationSerializable object) {
        Map<String, Object> serialized = new HashMap(object.serialize());
        for (Map.Entry<String, Object> entry : serialized.entrySet()) {
            if (entry.getValue() instanceof ConfigurationSerializable) {
                entry.setValue(serialize((ConfigurationSerializable) entry.getValue()));
            } else if (entry.getValue() instanceof List<?> && ((List<?>) entry.getValue()).get(0) instanceof ConfigurationSerializable) {
                entry.setValue(serializeCollection((Collection<ConfigurationSerializable>) entry.getValue()));
            }
        }
        serialized.put(ConfigurationSerialization.SERIALIZED_TYPE_KEY, ConfigurationSerialization.getAlias(object.getClass()));
        return serialized;
    }
    
    private static List<Map<String, Object>> serializeCollection(Collection<ConfigurationSerializable> objects) {
        List<Map<String, Object>> serialized = new ArrayList(objects.size());
        for (ConfigurationSerializable object : objects) {
            serialized.add(serialize(object));
        }
        return serialized;
    }
    
    private static List<ConfigurationSerializable> deserializeList(List<Map<String, Object>> serialized) {
        List<ConfigurationSerializable> deserialized = new ArrayList(serialized.size());
        
        for (Map<String, Object> entry : serialized) {
            deserialized.add(deserialize(entry));
        }
        
        return deserialized;
    }
    
    private static ConfigurationSerializable deserialize(Map<String, Object> serialized) {
        for (Map.Entry<String, Object> entry : serialized.entrySet()) {
            if (entry.getValue() instanceof Map && ((Map) entry.getValue()).containsKey(ConfigurationSerialization.SERIALIZED_TYPE_KEY)) {
                entry.setValue(deserialize((Map) entry.getValue()));
            } else if (entry.getValue() instanceof List && ((List) entry.getValue()).get(0) instanceof Map) {
                entry.setValue(deserializeList((List<Map<String, Object>>) entry.getValue()));
            }
        }
        return ConfigurationSerialization.deserializeObject(serialized);
    }
    
    public static ConfigurationSerializable deserializeFromBytes(byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            return null;
        }
        
        try (ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
                ObjectInputStream ois = new ObjectInputStream(bais)) {
            return (ConfigurationSerializable) deserialize((Map<String, Object>) ois.readObject());
        } catch (IOException | ClassNotFoundException ex) {
            DarkMarket.LOG.log(Level.SEVERE, "Failed to deserialize ItemMeta", ex);
        }
        return null;
    }

}
