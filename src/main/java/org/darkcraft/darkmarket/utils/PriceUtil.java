package org.darkcraft.darkmarket.utils;

import java.text.DecimalFormat;
import org.darkcraft.darkmarket.Vault;

/**
 * @author Acrobot
 */
public class PriceUtil {
    public static final double NO_PRICE = -1;
    public static final double FREE = 0;

    public static final String FREE_TEXT = "free";

    public static final char BUY_INDICATOR = 'b';
    public static final char SELL_INDICATOR = 's';
    
    public static final double FRACTION = 100.d;
    
    private static final DecimalFormat FORMAT = new DecimalFormat("0.##");

    /**
     * Gets the price from the text
     *
     * @param text      Text to check
     * @param indicator Price indicator (for example, B for buy)
     * @return price
     */
    public static double get(String text, char indicator) {
        String[] split = text.replace(" ", "").toLowerCase().split(":");
        String character = String.valueOf(indicator);

        for (String part : split) {
            if (!part.contains(character)) {
                continue;
            }

            part = part.replace(character, "");

            if (part.equals(FREE_TEXT)) {
                return FREE;
            }

            if (NumberUtil.isDouble(part)) {
                double price = Double.valueOf(part);

                if (price < 0) {
                    return NO_PRICE;
                } else if (price == 0) {
                    return FREE;
                } else {
                    return round(price);
                }
            }
        }

        return NO_PRICE;
    }
    
    
    /**
     * Returns rice rounded to the hundredth
     */
    public static double round(double price) {
        return Math.round(FRACTION * price) / FRACTION;
    }

    /**
     * Gets the buy price from te text
     *
     * @param text Text to check
     * @return Buy price
     */
    public static double getBuyPrice(String text) {
        return get(text, BUY_INDICATOR);
    }

    /**
     * Gets the sell price from te text
     *
     * @param text Text to check
     * @return Sell price
     */
    public static double getSellPrice(String text) {
        return get(text, SELL_INDICATOR);
    }
    
    /**
     * Returns price from text
     */
    public static double getPrice(String text) {
        if (NumberUtil.isFloat(text)) {
            double price = Double.parseDouble(text);
            
            if (price < 0) {
                return NO_PRICE;
            }
            
            return round(price);
        }
        return isFree(text)? FREE : NO_PRICE;
    }

    /**
     * Checks if the string is a valid price
     *
     * @param text Text to check
     * @return Is the string a valid price
     */
    public static boolean isPrice(String text) {
        return NumberUtil.isFloat(text) || isFree(text);
    }
    
    /**
     * Checks if the string is valid for free
     */ 
    public static boolean isFree(String text) {
        return text.trim().equalsIgnoreCase(FREE_TEXT);
    }
    
    /**
     * Returns formatted price with currency name appended
     */
    public static String formatWithCurrency(double price) {
        if (price == FREE) {
            return FREE_TEXT;
        }
        
        return Vault.getEconomy().format(price);
    }
    
    
    /**
     * Gets format of two prices for signs
     */
    public static String getSignFormat(double buy, double sell) {
        String line = "";
        if (buy > NO_PRICE) {
            line = "B " + (buy == 0? FREE_TEXT : FORMAT.format(buy));
        }       
        
        if (sell > NO_PRICE) {
            if (buy > NO_PRICE) {
                line += " : ";
            }
            line = line + (sell == 0? FREE_TEXT : FORMAT.format(sell)) + " S";
        }
        
        if (line.length() > 15) {
            line = line.replace(FREE_TEXT, String.valueOf(FREE));
        }
        
        if (line.length() > 15) {
            line = line.replace(" : ", ":");
        }
        
        if (line.length() > 15) {
            line = line.replace(" ", "");
        }
        
        return line;
    }
    
    /**
     * Returns two prices from a line
     */
    public static double[] getPrices(String line) {
        String[] split = line.replace(" ", "").split(":");
        
        return new double[]{
            isPrice(split[0]) ? getPrice(split[0]) : getBuyPrice(line), // buy
            split.length > 1 && isPrice(split[1]) ? getPrice(split[1]) : getSellPrice(line) // sell
        };
    }
    
}
