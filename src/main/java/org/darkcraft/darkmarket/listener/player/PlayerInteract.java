package org.darkcraft.darkmarket.listener.player;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.darkcraft.darkmarket.Config;
import org.darkcraft.darkmarket.Message;
import org.darkcraft.darkmarket.chest.ShopChest;
import org.darkcraft.darkmarket.event.PreTransactionEvent;
import org.darkcraft.darkmarket.event.TransactionEvent;
import org.darkcraft.darkmarket.shop.Shop;
import org.darkcraft.darkmarket.shop.ShopManager;
import org.darkcraft.darkmarket.shop.ShopSign;
import org.darkcraft.darkmarket.utils.uBlock;
import org.darkcraft.darkmarket.listener.block.Protection;
import org.darkcraft.darkmarket.listener.shop.ShopEdit;
import org.darkcraft.darkmarket.utils.PriceUtil;
import org.darkcraft.darkmarket.inventory.MultiChestInventory;

import static org.darkcraft.darkmarket.event.TransactionEvent.TransactionType;
import org.darkcraft.darkmarket.listener.inventory.BulkManager;
import static org.darkcraft.darkmarket.listener.transaction.PreTransactionConfirmer.sendItemInformation;
import org.darkcraft.darkmarket.utils.InventoryUtil;

public class PlayerInteract implements Listener {
    
    private static final String CHEST_ACCESS_DENIED = "Denied access to a shop chest";
    private static final String ACCESSED_SHOP_CHEST = "You just accessed a shop chest";
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Block block = event.getClickedBlock();
        Action action = event.getAction();
        
        if (block == null || action == Action.PHYSICAL) {
            return;
        }
        
        Player player = event.getPlayer();
        
        if (action == Action.RIGHT_CLICK_BLOCK && uBlock.isChest(block)) {
            if (!ShopChest.isShopChest(block)) {
                return;
            }
            
            if (!Protection.canAccessShopChest(player, block)) {
                Message.ERROR.send(player, CHEST_ACCESS_DENIED);
                event.setCancelled(true);
            } else {
                Message.WARNING.send(event.getPlayer(), ACCESSED_SHOP_CHEST);
            }
            
            return;
        }
        
        if (!uBlock.isSign(block)) {
            return;
        }
        
        Sign sign = (Sign) block.getState();
        
        if (!ShopSign.isValid(sign)) {
            return;
        }
        
        Shop shop = ShopManager.getShop(block.getLocation());
        if (shop == null) {
            return;
        }
        
        if (event.hasItem() && event.getItem().getType() == Material.SIGN && ShopEdit.canEditShop(player, shop)) {
            return;
        }
        
        if (shop.isOwner(player)) {
            if (action == Action.LEFT_CLICK_BLOCK) {
                sendShopInformation(player, shop);
                ShopSign.formatShopSign(shop, false);
                return;
            }
            
            if (shop.hasInventory()) {
                BulkManager.prepareShopDeposit(event.getPlayer(), shop);
            } else {
                Message.WARNING.send(player, "This shop isn't linked to any chests");
            }
            return;
        }
        
        if (event.getPlayer().getGameMode() == GameMode.CREATIVE) {
            return;
        }
        
        if (action == Action.RIGHT_CLICK_BLOCK ) {
            event.setCancelled(true);
        }
        
        PreTransactionEvent preEvent = prepareTransaction(shop, player, action);
        Bukkit.getPluginManager().callEvent(preEvent);
        
        if (preEvent.isCancelled()) {
            return;
        }
        
        TransactionEvent tevent = new TransactionEvent(preEvent);
        Bukkit.getPluginManager().callEvent(tevent);
    }
    
    public PreTransactionEvent prepareTransaction(Shop shop, Player client, Action action) {
        boolean buy = action == Action.LEFT_CLICK_BLOCK;
        
        TransactionType type = buy? TransactionType.BUY : TransactionType.SELL;
        
        ItemStack item = shop.getItem();
        int quantity = shop.getQuantity();
        
        double price = buy? shop.getBuyPrice() : shop.getSellPrice();
        
        if (Config.shiftSellsStack && client.isSneaking() && price > PriceUtil.NO_PRICE) {
            int amount = item.getMaxStackSize();
            
            if (quantity < amount) {
                price = (price / quantity) * amount;
                quantity = amount;
            }
        }
        
        return new PreTransactionEvent(shop, client, item, quantity, price, type);
    }
    
    public void sendShopInformation(Player player, Shop shop) {
        Message.INFO.send(player, "Shop information:");
        sendItemInformation(player, shop.getItem(), shop.getQuantity());
        
        if (shop.hasInventory()) {
            sendShopStockInformation(player, shop.getItem(), shop.getInventory());
        }
    }
    
    public void sendShopStockInformation(Player player, ItemStack item, MultiChestInventory inventory) {
        Message.INFO.sendList(player, 1, "Stock:");
        Message.INFO.sendList(player, 2, "Chests linked: " + inventory.getChestCount());
        Message.INFO.sendList(player, 2, "Items left: " + InventoryUtil.has(inventory.getInventories(), item));
        Message.INFO.sendList(player, 2, "Free space: " + InventoryUtil.space(inventory.getInventories(), item));
    }

}
