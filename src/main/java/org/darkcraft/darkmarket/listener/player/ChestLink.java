package org.darkcraft.darkmarket.listener.player;

import java.util.HashMap;
import java.util.Map;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.darkcraft.darkmarket.Config;
import org.darkcraft.darkmarket.DarkMarket;
import org.darkcraft.darkmarket.Message;
import org.darkcraft.darkmarket.chest.ChestLinker;
import org.darkcraft.darkmarket.chest.ChestLinker.Result;
import org.darkcraft.darkmarket.shop.Shop;
import org.darkcraft.darkmarket.utils.uBlock;

public class ChestLink implements Listener {
    
    private static final Map<String, ChestLinker> CHEST_LINKERS = new HashMap();
    
    public static void registerLinker(Player player, Shop shop) {
        addToMap(player, shop);
        Message.INFO.send(player, BEGIN_MESSAGE);
        Message.INFO.send(player, FINISH_ANY_TIME);
    }
    
    private static void addToMap(final Player player, final Shop shop) {
        new BukkitRunnable() {

            @Override
            public void run() {
                CHEST_LINKERS.put(player.getName(), new ChestLinker(shop));
            }
            
        }.runTaskLater(DarkMarket.getInstance(), 1L);
    }
    
    public static boolean isLinkingChests(Player player) {
        return CHEST_LINKERS.containsKey(player.getName());
    }
     
    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Block block = event.getClickedBlock();
        
        if (block == null || !uBlock.isChest(block) || !isLinkingChests(player)) {
            return;
        }
        
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            event.setCancelled(true);
        }
        
        clickedChest(player, block);
    }
    
    @EventHandler
    public void onPlayerChat(PlayerChatEvent event) {
        Player player = event.getPlayer();
        
        if (!CHEST_LINKERS.containsKey(player.getName())) {
            return;
        }
        
        String message = event.getMessage().toLowerCase();
        
        event.setCancelled(true);
        
        if (message.equals("cancel")) {
            CHEST_LINKERS.remove(player.getName());
            Message.INFO.send(player, CANCELLED);
            return;
        }
        
        if (message.equals("finish")) {
            ChestLinker linker = CHEST_LINKERS.get(player.getName());
            linker.finish();
            
            Message.INFO.send(player, String.format(COMPLETED, linker.getChestCount()));
            
            CHEST_LINKERS.remove(player.getName());
            return;
        }
        
        Message.WARNING.send(player, FINISH_ANY_TIME);
    }
    
    public void clickedChest(Player player, Block chest) {
        ChestLinker linker = CHEST_LINKERS.get(player.getName());
        Result result = linker.addChest(chest);
        
        String message = null;
        switch (result) {
            case ADDED:
                Message.INFO.send(player, String.format(LINKED_CHEST, chest.getX(), chest.getY(), chest.getZ(), linker.getChestCount()));
                break;
            case REMOVED:
                Message.INFO.send(player, String.format(UNLINKED_CHEST, chest.getX(), chest.getY(), chest.getZ(), linker.getChestCount()));
                break;
            case CHEST_OWNED:
                message = CHEST_IS_OWNED;
                break;
            case OUT_OF_RANGE:
                message = String.format(OUT_OF_RANGE, Config.chestRadius);
                break;
            case TOO_MANY_CHESTS:
                message = String.format(TOO_MANY_CHESTS, Config.maxLinkedChests);
        }
        
        if (message != null) {
            Message.WARNING.send(player, message);
        }
    }
    
    private static final String BEGIN_MESSAGE = "Please click the chests you would like to link to this shop";
    private static final String FINISH_ANY_TIME = "Type \"finish\" to save any changes, type \"cancel\" to discard them";
    
    private static final String COMPLETED = "Saved chests! There is now %d chests linked to your shop";
    private static final String CANCELLED = "Cancelled chest linking, no changes were saved";
    
    private static final String LINKED_CHEST = "Linked chest at (x: %d, y: %d, z: %d). Chests linked: %d";
    private static final String UNLINKED_CHEST = "Unlinked chest at (x: %d, y: %d, z: %d). Chests linked: %d";
    
    private static final String CHEST_IS_OWNED = "That chest is already owned, you cannot link a shop to it";
    private static final String OUT_OF_RANGE = "Chests must be within %d blocks of a shop";
    private static final String TOO_MANY_CHESTS = "You may only link up to %d chests to one shop";

}
