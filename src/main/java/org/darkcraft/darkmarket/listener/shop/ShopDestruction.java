package org.darkcraft.darkmarket.listener.shop;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.darkcraft.darkmarket.DarkMarket;
import org.darkcraft.darkmarket.Message;
import org.darkcraft.darkmarket.db.Chests;
import org.darkcraft.darkmarket.event.ShopDestructionEvent;
import org.darkcraft.darkmarket.listener.block.Protection;
import org.darkcraft.darkmarket.shop.ShopManager;

import static org.darkcraft.darkmarket.listener.transaction.TransactionLogger.locationToString;

public class ShopDestruction implements Listener {
    
    private static final String DESTROYED_SHOP_PLAYER = "Shop destroyed!";
    private static final String DESTROYED_SHOP_CONSOLE = "Shop at %s destroyed by %s";
    
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onShopWipe(ShopDestructionEvent event) {
        if (event.getShop().hasInventory()) {
            Chests.clearShopChests(event.getShop());
        }
        
        ShopManager.deleteShop(event.getShop());
        
        if (event.isPlayerAction()) {
            Message.INFO.send(event.getPlayer(), DESTROYED_SHOP_PLAYER);
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onConsoleLog(ShopDestructionEvent event) {
        Location location = event.getShop().getSign().getLocation();
        DarkMarket.LOG.info(String.format(DESTROYED_SHOP_CONSOLE, locationToString(location), event.isPlayerAction()? event.getPlayer().getName() : "UNKNOWN"));
    }
    
    @EventHandler
    public void onShopDestroy(ShopDestructionEvent event) {
        if (!event.isPlayerAction()) {
            return;
        }
        
        Player player = event.getPlayer();
        if (player.getGameMode() == GameMode.CREATIVE) {
            Message.WARNING.send(player, Protection.CREATIVE_BREAK);
            event.setCancelled(true);
        }
        
        if (!Protection.canBreakShop(player, event.getShop())) {
            Message.WARNING.send(player, Protection.NOT_ALLOWED_TO_BREAK);
            event.setCancelled(true);
        }
    }

}
