package org.darkcraft.darkmarket.listener.shop;

import java.util.HashMap;
import java.util.Map;
import net.milkbowl.vault.item.Items;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.darkcraft.darkmarket.Message;
import org.darkcraft.darkmarket.event.DirectInputEvent;
import org.darkcraft.darkmarket.event.PreShopCreationEvent;
import org.darkcraft.darkmarket.event.PreShopCreationEvent.Input;
import org.darkcraft.darkmarket.listener.block.SignChange;
import org.darkcraft.darkmarket.shop.ShopSign;
import org.darkcraft.darkmarket.utils.NumberUtil;
import org.darkcraft.darkmarket.utils.PriceUtil;
import org.darkcraft.darkmarket.event.ShopEditEvent;

import static org.darkcraft.darkmarket.event.PreShopCreationEvent.Input.QUANTITY;

public class DirectInput implements Listener {
    
    private Map<String, DirectInputEvent> DIRECT_INPUTS = new HashMap();
    
    @EventHandler
    public void onDirectInputEvent(DirectInputEvent event) {
        PreShopCreationEvent psce = event.getPreShopCreationEvent();
        DIRECT_INPUTS.put(psce.getCreator().getName(), event);
        getNextInput(event);
    }
    
    public void getNextInput(DirectInputEvent event) {
        if (event.getRemainingInput().isEmpty()) {
            finalize(event);
            return;
        }
        
        Input next = event.getRemainingInput().get(0);
        String message = null;
        switch (next) {
            case ITEM:
                message = "Please punch the sign with item you wish to sell.";
                break;
            case QUANTITY:
                message = "Please say a quantity.";
                break;
            case PRICE:
                message = "Please say a price in the following format: B $:$ S";
        }
        
        Message.INFO.send(event.getPreShopCreationEvent().getCreator(), message);
        event.setCurrentInput(next);
    }
    
    public void finalize(DirectInputEvent event) {
        PreShopCreationEvent psce = event.getPreShopCreationEvent();
        DIRECT_INPUTS.remove(psce.getCreator().getName());
       
        if (psce.isShopEditEvent()) {
            ((ShopEditEvent) psce).saveEdits();
        } else {
            SignChange.callShopCreatedEvent(psce);
        }
    }
    
    public void cancel(DirectInputEvent event) {
        PreShopCreationEvent psce = event.getPreShopCreationEvent();
        DIRECT_INPUTS.remove(psce.getCreator().getName());
        Message.INFO.send(psce.getCreator(), CANCELLED);
        psce.getSign().getBlock().breakNaturally();
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (!DIRECT_INPUTS.containsKey(player.getName())) {
            return;
        }
        
        DirectInputEvent die = DIRECT_INPUTS.get(player.getName());
        if (die.getCurrentInput() != Input.ITEM) {
            return;
        }
        
        ItemStack item = event.getItem();
        if (item == null) {
            Message.WARNING.send(player, EMPTY_HAND);
            return;
        }
        
        die.getPreShopCreationEvent().setItem(item);
        die.completeInput(Input.ITEM);
        Message.INFO.send(player, "> " + Items.itemByStack(item).getName());
        getNextInput(die);
    }
    
    @EventHandler
    public void onPlayerChat(PlayerChatEvent event) {
        Player player = event.getPlayer();
        if (!DIRECT_INPUTS.containsKey(player.getName())) {
            return;
        }
        DirectInputEvent die = DIRECT_INPUTS.get(player.getName());
        
        event.setCancelled(true);
        
        if (event.getMessage().isEmpty()) {
            getNextInput(die);
            return;
        }
        
        if (event.getMessage().equalsIgnoreCase("cancel")) {
            cancel(die);
            return;
        }
        
        if (die.getCurrentInput() == Input.QUANTITY) {
            String[] part = event.getMessage().split(" ");
            
            if (!NumberUtil.isInteger(part[0]) || part[0].length() > 15) {
                Message.WARNING.send(player, QUANTITY_INVALID);
                return;
            }
            
            die.getPreShopCreationEvent().setQuantity(Integer.parseInt(part[0]));
            die.completeInput(Input.QUANTITY);
            Message.INFO.send(player, "> " + part[0]);
            getNextInput(die);
            return;
        }
        
        if (die.getCurrentInput() == Input.PRICE) {
            String line = event.getMessage();
            
            if (!ShopSign.isValid(line, ShopSign.PRICE_LINE)) {
                Message.WARNING.send(player, PRICE_INVALID);
                return;
            }
            
            double[] prices = PriceUtil.getPrices(line);
            double buy = prices[0], sell = prices[1];
        
            line = PriceUtil.getSignFormat(buy, sell);
            if (line.length() > 15) {
                Message.WARNING.send(player, PRICE_TOO_LARGE);
                return;
            }
            
            if (buy > PriceUtil.NO_PRICE && buy < sell) {
                Message.WARNING.send(player, PRICE_RATIO);
                return;
            }
            
            PreShopCreationEvent psce = die.getPreShopCreationEvent();
            psce.setBuyPrice(buy);
            psce.setSellPrice(sell);
            die.completeInput(Input.PRICE);
            Message.INFO.send(player, "> " + line);
            getNextInput(die);
            return;
        }
        
        Message.WARNING.send(player, CANCEL_ANY_TIME);
    }
    
    private static final String EMPTY_HAND = "You must be holding something in your hand.";
    private static final String QUANTITY_INVALID = "Quantity invalid. Please choose any number above 0.";
    private static final String PRICE_INVALID = "That price is invalid.";
    private static final String PRICE_TOO_LARGE = "Those prices are too large.";
    private static final String PRICE_RATIO = "The buying price must be less than the selling price";
    private static final String CANCEL_ANY_TIME = "Type \"cancel\" in chat to abort at any time.";
    private static final String CANCELLED = "Cancelled shop creation!";

}
