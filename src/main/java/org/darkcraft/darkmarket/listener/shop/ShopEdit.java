package org.darkcraft.darkmarket.listener.shop;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.darkcraft.darkmarket.Permission;
import org.darkcraft.darkmarket.event.ShopEditEvent;
import org.darkcraft.darkmarket.shop.Shop;
import org.darkcraft.darkmarket.event.PreShopCreationEvent.State;

import static org.darkcraft.darkmarket.event.PreShopCreationEvent.Input;
import static org.darkcraft.darkmarket.shop.ShopSign.HEADER_LINE;
import static org.darkcraft.darkmarket.shop.ShopSign.ITEM_LINE;

public class ShopEdit implements Listener {
    
    @EventHandler
    public void onShopEdit(ShopEditEvent event) {
        if (!event.hasLine(HEADER_LINE)) {
            return;
        }
        String line = event.getSignLine(HEADER_LINE).trim();
        
        if (line.toLowerCase().startsWith("!chest")) {
            event.setEditingChests(true);
            return;
        }
        
        if (!canEditShopOwner(event.getCreator(), event.getShop())) {
            return;
        }
        
        String name = line.replaceAll("[^\\w]", "");
        if (line.isEmpty()) {
            return;
        }
        
        event.setOwner(Bukkit.getOfflinePlayer(name));
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public void onItemCheck(ShopEditEvent event) {
        if (!event.hasLine(ITEM_LINE)) {
            event.getRemainingInput().remove(Input.ITEM);
            return;
        }
        
        if (event.getState() == State.INVALID_ITEM && event.getSignLine(ITEM_LINE).trim().equals("!")) {
            event.setState(State.SHOP_CREATED_SUCCESSFULLY);
        }
    }
    
    private static boolean canEditShopOwner(Player player, Shop shop) {
        return Permission.ADMIN.has(player);
    }
    
    public static boolean canEditShop(Player player, Shop shop) {
        return Permission.ADMIN.has(player) || player.getName().equals(shop.getOwnerName());
    }
    
}
