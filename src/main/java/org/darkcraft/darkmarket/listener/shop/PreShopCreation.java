package org.darkcraft.darkmarket.listener.shop;

import net.milkbowl.vault.item.ItemInfo;
import net.milkbowl.vault.item.Items;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.darkcraft.darkmarket.event.PreShopCreationEvent;
import org.darkcraft.darkmarket.shop.ShopSign;
import org.darkcraft.darkmarket.utils.PriceUtil;
import org.darkcraft.darkmarket.Permission;

import static org.darkcraft.darkmarket.Message.ERROR;
import static org.darkcraft.darkmarket.event.PreShopCreationEvent.State;
import static org.darkcraft.darkmarket.event.PreShopCreationEvent.Input;

import static org.darkcraft.darkmarket.shop.ShopSign.ITEM_LINE;
import static org.darkcraft.darkmarket.shop.ShopSign.QUANTITY_LINE;
import static org.darkcraft.darkmarket.shop.ShopSign.PRICE_LINE;
import static org.darkcraft.darkmarket.shop.ShopSign.HEADER_LINE;

public class PreShopCreation implements Listener {
    
    @EventHandler
    public void onAdminShopCheck(PreShopCreationEvent event) {
        if (event.isShopEditEvent() || !ShopSign.isAdminShop(event.getSignLine(HEADER_LINE))) {
            return;
        }
        
        if (!Permission.CREATE_SHOP_ADMIN.has(event.getCreator())) {
            event.setState(State.NO_PERMISSION);
            return;
        }
        
        event.setAdminShop(true);
    }
    
    @EventHandler
    public void onItemCheck(PreShopCreationEvent event) {
        if (!event.hasLine(ITEM_LINE)) {
            return;
        }
        
        String line = event.getSignLine(ITEM_LINE);
        
        if (!ShopSign.isValid(line, ITEM_LINE)) {
            event.setState(State.INVALID_ITEM);
            return;
        }
        
        ItemInfo itemInfo = Items.itemByString(line);
        
        if (itemInfo == null) {
            event.setState(State.INVALID_ITEM);
            return;
        }

        event.setItem(itemInfo.toStack());
        event.removeInput(Input.ITEM);
    }
    
    @EventHandler
    public void onQuantityCheck(PreShopCreationEvent event) {
        if (!event.hasLine(QUANTITY_LINE)) {
            return;
        }
        
        String line = event.getSignLine(QUANTITY_LINE);
        
        if (!ShopSign.isValid(line, QUANTITY_LINE)) {
            event.setState(State.INVALID_QUANTITY);
            return;
        }
        
        int amount = Integer.parseInt(line);
        if (amount < 1) {
            event.setState(State.INVALID_QUANTITY);
            return;
        }
        
        event.setQuantity(amount);
        event.removeInput(Input.QUANTITY);
    }
    
    @EventHandler
    public void onPriceCheck(PreShopCreationEvent event) {
        if (!event.hasLine(PRICE_LINE)) {
            return;
        }
        
        String line = event.getSignLine(PRICE_LINE);
        
        if (!ShopSign.isValid(line, PRICE_LINE)) {
            event.setState(State.INVALID_PRICE);
            return;
        }

        double[] prices = PriceUtil.getPrices(line);
        
        event.setBuyPrice(prices[0]);
        event.setSellPrice(prices[1]);
        
        line = PriceUtil.getSignFormat(event.getBuyPrice(), event.getSellPrice());
        if (line.length() > 15) {
            event.setState(State.INVALID_PRICE);
            return;
        }
        
        event.removeInput(Input.PRICE);
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public void onPriceRatioCheck(PreShopCreationEvent event) {
        if (event.getBuyPrice() > PriceUtil.NO_PRICE && event.getBuyPrice() < event.getSellPrice()) {
            event.setState(State.SELL_PRICE_HIGHER_THAN_BUY_PRICE);
        }
    }
    
    @EventHandler(priority = EventPriority.LOW)
    public void onPermissionCheck(PreShopCreationEvent event) {
        if (Permission.CREATE_SHOP.has(event.getCreator())) {
            return;
        }
        
        event.setState(State.NO_PERMISSION);
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onMessageSend(PreShopCreationEvent event) {
        if (!event.isCancelled()) {
            return;
        }
        
        String message = null;
        switch(event.getState()) {
            case INVALID_ITEM:
                message = "Invalid item ID detected";
                break;
            case INVALID_PRICE:
            case INVALID_QUANTITY:
                message = "Invalid shop detected";
                break;
            case NO_PERMISSION:
                message = "You don't have permission to create that shop";
                break;
            case SELL_PRICE_HIGHER_THAN_BUY_PRICE:
                message = "The buying price must be less than the selling price";
                break;
        }
        
        if (message != null) {
            ERROR.send(event.getCreator(), message);
        }
    }
    
}
