package org.darkcraft.darkmarket.listener.shop;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.darkcraft.darkmarket.Message;
import org.darkcraft.darkmarket.event.ShopCreationEvent;
import org.darkcraft.darkmarket.shop.Shop;
import org.darkcraft.darkmarket.shop.ShopManager;
import org.darkcraft.darkmarket.shop.ShopSign;

import static org.bukkit.event.EventPriority.MONITOR;
import static org.darkcraft.darkmarket.listener.player.ChestLink.registerLinker;

public class ShopCreation implements Listener {
    
    private static final String SHOP_CREATED = "Shop successfully created!";
    
    @EventHandler
    public void onShopCreation(ShopCreationEvent event) {
        Message.INFO.send(event.getCreator(), SHOP_CREATED);
    }
    
    @EventHandler(priority = MONITOR)
    public void onShopSave(ShopCreationEvent event) {
        Shop shop = ShopManager.createShop(event);
        
        ShopSign.formatShopSign(shop, false);
        
        if (!shop.isAdminShop()) {
            registerLinker(event.getCreator(), shop);
        }
    }

}
