package org.darkcraft.darkmarket.listener.block;

import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.darkcraft.darkmarket.Message;
import org.darkcraft.darkmarket.Permission;
import org.darkcraft.darkmarket.chest.ShopChest;
import org.darkcraft.darkmarket.event.ShopDestructionEvent;
import org.darkcraft.darkmarket.shop.Shop;
import org.darkcraft.darkmarket.shop.ShopManager;
import org.darkcraft.darkmarket.shop.ShopSign;
import org.darkcraft.darkmarket.utils.uBlock;

public class Protection implements Listener {
    
    public static final String NOT_ALLOWED_TO_BREAK = "You cannot break that block";
    public static final String CREATIVE_BREAK = "You cannot break a shop in creative mode";
    public static final String UNLINK_FIRST = "You must unlink the following shops from this chest before it can be broken:";
    public static final String SHOP_LOCATION = "Shop at (x: %d, y: %d, z: %d)";
    public static final String CANNOT_JOIN_CHEST = "You cannot place a chest there";
    
    @EventHandler(ignoreCancelled = true)
    public void onShopBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        if (block == null) {
            return;
        }
        
        if (isShopAttachedToBlock(event.getBlock())) {
            Message.WARNING.send(event.getPlayer(), NOT_ALLOWED_TO_BREAK);
            event.setCancelled(true);
            return;
        }
        
        if (!uBlock.isSign(block)) {
            return;
        }
        
        Sign sign = (Sign) block.getState();
        
        if (!ShopSign.isValid(sign)) {
            return;
        }
        
        Player player = event.getPlayer();
        
        Shop shop = ShopManager.getShop(block.getLocation());
        if (shop == null) {
            return;
        }
        
        ShopDestructionEvent shopBreak = new ShopDestructionEvent(player,shop);
        Bukkit.getPluginManager().callEvent(shopBreak);
        
        if (shopBreak.isCancelled()) {
            event.setCancelled(true);
        }
    }
    
    @EventHandler(ignoreCancelled = true)
    public void onChestBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        
        if (block == null || !uBlock.isChest(block) || !ShopChest.isShopChest(block)) {
            return;
        }
        
        if (!canAccessShopChest(event.getPlayer(), block)) {
            Message.WARNING.send(event.getPlayer(), NOT_ALLOWED_TO_BREAK);
            event.setCancelled(true);
            return;
        }
        
        List<Shop> shops = ShopChest.getShopsByChest(block);
        
        Message.INFO.send(event.getPlayer(), UNLINK_FIRST);
        for (Shop shop : shops) {
            Message.INFO.send(event.getPlayer(), String.format(SHOP_LOCATION, shop.getSign().getX(), shop.getSign().getY(), shop.getSign().getZ()));
        }
        
        event.setCancelled(true);
    }
    
    @EventHandler(ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        Block block = event.getBlock();
        
        if (block == null || !uBlock.isChest(block)) {
            return;
        }
        
        Block partner = uBlock.getChestPair(block);
        if (partner == null || !ShopChest.isShopChest(partner) || canAccessShopChest(event.getPlayer(), partner)) {
            return;
        }
        
        event.setCancelled(true);
        Message.WARNING.send(event.getPlayer(), CANNOT_JOIN_CHEST);
    }
    
    @EventHandler
    public void onBlockPhysics(BlockPhysicsEvent event) {
        Block block = event.getBlock();
        
        if (block == null || !uBlock.isSign(block) || !ShopSign.isValid(block)) {
            return;
        }
        
        event.setCancelled(true);
    }
    
    @EventHandler
    public void onPistonExtend(BlockPistonExtendEvent event) {
        for (Block block : event.getBlocks()) {
             if (isShopAttachedToBlock(block)) {
                 event.setCancelled(true);
                 return;
             }
        }
    }
    
    @EventHandler
    public void onPistonRetract(BlockPistonRetractEvent event) {
        if (!event.isSticky() || !isShopAttachedToBlock(event.getBlock())) {
            return;
        }

        event.setCancelled(true);
    }
    
    public static boolean isShopAttachedToBlock(Block block) {
        List<Sign> signs = uBlock.getAttachedSigns(block);
        
        if (signs.isEmpty()) {
            return false;
        }
        
        for (Sign sign : signs) {
            if (ShopSign.isHeaderValid(sign.getLine(ShopSign.HEADER_LINE))) {
                return true;
            }
        }
        
        return false;
    }
    
    public static boolean canBreakShop(Player player, Shop shop) {
        return Permission.MOD.has(player) || shop.getOwnerName().equals(player.getName());
    }
    
    public static boolean canAccessShopChest(Player player, Block chest) {
        return Permission.MOD.has(player) || ShopChest.canPlayerAccess(player, chest);
    }

}
