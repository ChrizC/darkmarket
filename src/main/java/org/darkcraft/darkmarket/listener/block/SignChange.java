package org.darkcraft.darkmarket.listener.block;

import java.util.HashMap;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.darkcraft.darkmarket.event.DirectInputEvent;
import org.darkcraft.darkmarket.event.PreShopCreationEvent;
import org.darkcraft.darkmarket.event.ShopCreationEvent;
import org.darkcraft.darkmarket.event.ShopEditEvent;
import static org.darkcraft.darkmarket.listener.shop.ShopEdit.canEditShop;
import org.darkcraft.darkmarket.shop.Shop;
import org.darkcraft.darkmarket.shop.ShopManager;
import org.darkcraft.darkmarket.shop.ShopSign;
import org.darkcraft.darkmarket.utils.uBlock;
import static org.darkcraft.darkmarket.utils.uBlock.isSign;

public class SignChange implements Listener {
        
    private static final Map<String, Block> EDITORS = new HashMap();
    
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Block block = event.getBlock();
        Block against = event.getBlockAgainst();
        
        if (!isSign(block) || !isSign(against) || !ShopSign.isValid(against)) {
            return;
        }
        
        EDITORS.put(event.getPlayer().getName(), against);
    }
    
    @EventHandler
    public void onSignCreate(SignChangeEvent event) {
        Block block = event.getBlock();
        Player player = event.getPlayer();
        
        if (block == null || !uBlock.isSign(block)) {
            return;
        }
        
        if (EDITORS.containsKey(player.getName())) {
            callShopEditEvent(event);
            return;
        }
        
        if (!ShopSign.isHeaderValid(event.getLine(ShopSign.HEADER_LINE))) {
            return;
        }

        PreShopCreationEvent psce = new PreShopCreationEvent(player, (Sign) block.getState(), event.getLines());
        Bukkit.getPluginManager().callEvent(psce);
        
        if (psce.isCancelled()) {
            event.setCancelled(true);
            return;
        }
        
        if (psce.requiresInput()) {
            DirectInputEvent die = new DirectInputEvent(psce);
            Bukkit.getPluginManager().callEvent(die);
            return;
        }
        
        ShopCreationEvent sce = callShopCreatedEvent(psce);
        
        String[] lines = ShopSign.getShopSignFormat(sce.isAdminShop(), sce.getItem(), sce.getQuantity(), sce.getBuyPrice(), sce.getSellPrice());
        
        for (int i = 0; i < lines.length; i++) {
            event.setLine(i, lines[i]);
        }
    }
    
    
    public void callShopEditEvent(SignChangeEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();
        
        Block against = EDITORS.get(player.getName());
        
        boolean notEmpty = false;
        for (int i = 0; i < event.getLines().length; i++) {
            if (!event.getLine(i).isEmpty()) {
                notEmpty = true;
                break;
            }
        }
        
        if (!notEmpty) {
            return;
        }
        
        Shop shop = ShopManager.getShop(against.getLocation());
        if (shop == null) {
            return;
        }
        
        if (!canEditShop(player, shop)) {
            uBlock.destroyLater(block, 1L);
            return;
        }
        
        ShopEditEvent editEvent = new ShopEditEvent(player, shop, (Sign) block.getState(), event.getLines());
        
        Bukkit.getPluginManager().callEvent(editEvent);
        
        if (editEvent.isCancelled()) {
            uBlock.destroyLater(block, 1L);
            return;
        }
        
        editEvent.getRemainingInput().remove(PreShopCreationEvent.Input.PRICE);
        editEvent.getRemainingInput().remove(PreShopCreationEvent.Input.QUANTITY);
        
        if (editEvent.requiresInput()) {
            DirectInputEvent die = new DirectInputEvent(editEvent);
            Bukkit.getPluginManager().callEvent(die);
            return;
        }
        
        editEvent.saveEdits();
        uBlock.destroyLater(block, 1L);
    }
    
    public static ShopCreationEvent callShopCreatedEvent(PreShopCreationEvent event) {
        ShopCreationEvent sce = new ShopCreationEvent(event);
        Bukkit.getPluginManager().callEvent(sce);
        return sce;
    }
    
    public static void removeEditor(Player player) {
        EDITORS.remove(player.getName());
    }

}
