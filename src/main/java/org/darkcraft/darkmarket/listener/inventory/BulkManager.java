package org.darkcraft.darkmarket.listener.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.darkcraft.darkmarket.Message;
import org.darkcraft.darkmarket.shop.Shop;
import org.darkcraft.darkmarket.utils.InventoryUtil;
import org.darkcraft.darkmarket.utils.ItemStackUtil;

import static org.darkcraft.darkmarket.listener.transaction.TransactionMessenger.formatItem;

public class BulkManager implements Listener {
    
    private static final HashMap<String, Shop> DEPOSITERS = new HashMap();
    
    private static final String DEPOSIT_TITLE = "Shop Deposit";
    
    private static final String DEPOSIT_MESSAGE = "Deposited %s into your shop";
    
    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        Inventory inventory = event.getInventory();
        HumanEntity player = event.getPlayer();
        
        if (!inventory.getTitle().equals(DEPOSIT_TITLE) || !DEPOSITERS.containsKey(player.getName())) {
            return;
        }
        
        Shop shop = DEPOSITERS.get(player.getName());
        ItemStack item = shop.getItem();
        
        final List<Inventory> shopInventory = shop.getInventory().getInventories();
        
        int depositAmount = 0;
        int space = InventoryUtil.space(shopInventory, item);
        
        ArrayList<ItemStack> notFit = new ArrayList(inventory.getSize());
        
        for (ItemStack is : inventory.getContents()) {
            if (is == null) {
                continue;
            }
            
            if (!ItemStackUtil.equals(item, is) || space <= depositAmount) {
                notFit.add(is);
                continue;
            }
            
            if ((depositAmount += is.getAmount()) > space) {
                is.setAmount(space - depositAmount);
                notFit.add(is);
                depositAmount = space;
            }
        }
        
        InventoryUtil.addItem(shopInventory, item, depositAmount);
        Message.INFO.send((Player) player, String.format(DEPOSIT_MESSAGE, formatItem(item, depositAmount)));
        DEPOSITERS.remove(player.getName());
        
        if (notFit.isEmpty()) {
            return;
        }
        
        HashMap<Integer, ItemStack> left = player.getInventory().addItem(notFit.toArray(new ItemStack[notFit.size()]));
        
        InventoryUtil.updateInventoryLater((Player) player);
        
        if (left.isEmpty()) {
            return;
        }
        
        for (ItemStack is : left.values()) {
            player.getWorld().dropItem(player.getLocation(), is);
        }
    }
    
    
    public static void prepareShopDeposit(Player player, Shop shop) {
        DEPOSITERS.put(player.getName(), shop);
        
        Inventory deposit = Bukkit.createInventory(player, InventoryType.CHEST.getDefaultSize(), DEPOSIT_TITLE);
        player.openInventory(deposit);
    }

}
