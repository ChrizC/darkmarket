package org.darkcraft.darkmarket.listener.transaction;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.darkcraft.darkmarket.DarkMarket;
import org.darkcraft.darkmarket.Vault;
import org.darkcraft.darkmarket.event.PreTransactionEvent;
import org.darkcraft.darkmarket.utils.PriceUtil;

import static org.darkcraft.darkmarket.Message.ERROR;
import org.darkcraft.darkmarket.Permission;
import static org.darkcraft.darkmarket.event.TransactionEvent.TransactionType;
import static org.darkcraft.darkmarket.event.PreTransactionEvent.State;
import org.darkcraft.darkmarket.shop.Shop;

import org.darkcraft.darkmarket.utils.InventoryUtil;

public class PreTransaction implements Listener {

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPriceValidate(PreTransactionEvent event) {
        if (event.getPrice() > PriceUtil.NO_PRICE) {
            return;
        }

        if (event.getType() == TransactionType.BUY) {
            event.setState(State.SHOP_DOES_NOT_SELL_THIS_ITEM);
        } else {
            event.setState(State.SHOP_DOES_NOT_BUY_THIS_ITEM);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onShopInventoryCheck(PreTransactionEvent event) {
        if (event.getShop().isAdminShop() || event.getShop().hasInventory()) {
            return;
        }

        event.setState(State.SHOP_HAS_NO_INVENTORY);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOW)
    public void onPlayerMoneyCheck(PreTransactionEvent event) {
        if (event.getType() != TransactionType.BUY) {
            return;
        }

        if (!Vault.getEconomy().has(event.getClient().getName(), event.getPrice())) {
            event.setState(State.CLIENT_DOES_NOT_HAVE_ENOUGH_MONEY);
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOW)
    public void onShopMoneyCheck(PreTransactionEvent event) {
        if (event.getType() != TransactionType.SELL) {
            return;
        }

        if (!event.getShop().isAdminShop() && !Vault.getEconomy().has(event.getShop().getOwner().getName(), event.getPrice())) {
            event.setState(State.SHOP_DOES_NOT_HAVE_ENOUGH_MONEY);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onShopSellItemCheck(PreTransactionEvent event) {
        if (event.getType() != TransactionType.BUY) {
            return;
        }
        Shop shop = event.getShop();
        ItemStack item = event.getItem();
        int quantity = event.getQuantity();

        if (!shop.isAdminShop() && InventoryUtil.has(shop.getInventory().getInventories(), item) < quantity) {
            event.setState(State.NOT_ENOUGH_STOCK_IN_SHOP);
            return;
        }

        if (!InventoryUtil.fits(event.getClient().getInventory().getContents(), item, quantity)) {
            event.setState(State.NOT_ENOUGH_SPACE_IN_INVENTORY);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onShopBuyItemCheck(PreTransactionEvent event) {
        if (event.getType() != TransactionType.SELL) {
            return;
        }

        Shop shop = event.getShop();
        ItemStack item = event.getItem();
        int quantity = event.getQuantity();

        if (!InventoryUtil.contains(event.getClient().getInventory().getContents(), item, quantity)) {
            event.setState(State.NOT_ENOUGH_STOCK_IN_INVENTORY);
            return;
        }

        if (!shop.isAdminShop() && InventoryUtil.space(shop.getInventory().getInventories(), item) < quantity) {
            event.setState(State.NOT_ENOUGH_SPACE_IN_SHOP);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPermissionCheck(PreTransactionEvent event) {
        if (Permission.USE_SHOP.has(event.getClient())) {
            return;
        }

        event.setState(State.NO_PERMISSION);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onMessageSend(PreTransactionEvent event) {
        if (!event.isCancelled() || event.getState() == State.CONFIRMATION_REQUIRED) {
            return;
        }

        String message = null;
        switch (event.getState()) {
            case NO_PERMISSION:
                message = "You don't have permission to buy from shops";
                break;
            case SHOP_HAS_NO_INVENTORY:
                message = "This shop isn't linked to any chests";
                break;
            case SHOP_DOES_NOT_BUY_THIS_ITEM:
                message = "This shop doesn't buy items";
                break;
            case SHOP_DOES_NOT_SELL_THIS_ITEM:
                message = "This shop doesn't sell items";
                break;
            case CLIENT_DOES_NOT_HAVE_ENOUGH_MONEY:
                message = "You can't afford this";
                break;
            case SHOP_DOES_NOT_HAVE_ENOUGH_MONEY:
                message = "The shop owner can't afford that";
                break;
            case NOT_ENOUGH_SPACE_IN_SHOP:
                message = "There isn't enough space in the shop's inventory";
                break;
            case NOT_ENOUGH_SPACE_IN_INVENTORY:
                message = "There isn't enough space in your inventory";
                break;
            case NOT_ENOUGH_STOCK_IN_SHOP:
                message = "The shop doesn't have enough items";
                break;
            case NOT_ENOUGH_STOCK_IN_INVENTORY:
                message = "You don't have enough items";
        }

        if (message != null) {
            ERROR.send(event.getClient(), message);
        }
    }
}
