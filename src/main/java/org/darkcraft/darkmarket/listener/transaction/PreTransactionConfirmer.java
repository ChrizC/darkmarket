package org.darkcraft.darkmarket.listener.transaction;

import java.util.Map;
import java.util.WeakHashMap;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.Repairable;
import org.bukkit.inventory.meta.SkullMeta;
import org.darkcraft.darkmarket.Config;
import org.darkcraft.darkmarket.DarkMarket;
import org.darkcraft.darkmarket.event.PreTransactionEvent;
import org.darkcraft.darkmarket.utils.ItemUtil;
import org.darkcraft.darkmarket.utils.NumberUtil;
import org.darkcraft.darkmarket.utils.StringUtil;
import org.darkcraft.darkmarket.utils.PriceUtil;

import static org.darkcraft.darkmarket.event.PreTransactionEvent.State.CONFIRMATION_REQUIRED;
import static org.darkcraft.darkmarket.Message.INFO;

public class PreTransactionConfirmer implements Listener {
    
    private static final Map<String, PreTransactionEvent> INTERACTIONS = new WeakHashMap();
    
    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPreTransaction(PreTransactionEvent event) {
        Player client = event.getClient();
        
        if (INTERACTIONS.containsKey(client.getName())) {
            PreTransactionEvent pte = INTERACTIONS.get(client.getName());
            
            if (pte.getType() == event.getType() && pte.getShop().equals(event.getShop())) {
                return;
            }
        }
        
        sendTransactionInformation(event);
        sendConfirm(client);
                
        event.setState(CONFIRMATION_REQUIRED);
        INTERACTIONS.put(client.getName(), event);
    }
        
    private static void sendConfirm(Player player) {
        INFO.send(player, "Click again to complete the transaction");
    }
    
    private static void sendTransactionInformation(PreTransactionEvent event) {
        Player client = event.getClient();
        
        INFO.send(client, "Transaction with: " + event.getShop().getOwnerName());
        INFO.sendList(client, 1, "Type: " + event.getType().toString());
        INFO.sendList(client, 1, "Price: " + PriceUtil.formatWithCurrency(event.getPrice()));
        sendItemInformation(client, event.getItem(), event.getQuantity());
    }
    
    public static void sendItemInformation(Player player, ItemStack item, int amount) {
        INFO.sendList(player, 1, "Item: " + ItemUtil.getItemName(item));
        INFO.sendList(player, 2, "Amount: " + amount);
        
        if (!item.hasItemMeta()) {
            return;
        }
        
        ItemMeta meta = item.getItemMeta();
        
        if (meta.hasDisplayName()) {
            INFO.sendList(player, 2, "Name: " + meta.getDisplayName());
        }
        
        if (!Config.ignoreRepairCost) {
            Repairable repairable = (Repairable) meta;
            if (repairable.hasRepairCost()) {
                INFO.sendList(player, 2, "Repair cost: " + ((Repairable) meta).getRepairCost());
            }
        }
        
        if (meta.hasEnchants()) {
            sendEnchantments(player, meta.getEnchants());
        } else if (meta instanceof EnchantmentStorageMeta) {
            sendEnchantments(player, ((EnchantmentStorageMeta) meta).getStoredEnchants());
            
        } else if (meta instanceof BookMeta) {
            sendBookInformation(player, (BookMeta) meta);
            
        } else if (meta instanceof LeatherArmorMeta) {
            sendLeatherArmorInformation(player, (LeatherArmorMeta) meta);
            
        } else if (meta instanceof FireworkMeta) {
            sendFireworkInformation(player, (FireworkMeta) meta);
            
        } else if (meta instanceof FireworkEffectMeta) {
            sendFireworkStarInformation(player, (FireworkEffectMeta) meta);
            
        } else if (meta instanceof SkullMeta) {
            SkullMeta skull = (SkullMeta) meta;
            INFO.sendList(player, 2, "Skull owner: " + (skull.hasOwner()? skull.getOwner() : ""));
        }
    }
    
    private static void sendEnchantments(Player player, Map<Enchantment, Integer> enchantments) {
        INFO.sendList(player, 2, "Enchantments:");
        for (Map.Entry<Enchantment, Integer> entry : enchantments.entrySet()) {
            INFO.sendList(player, 3, StringUtil.capitalizeFirst(entry.getKey().getName()) + "  " + NumberUtil.toRoman(entry.getValue()));
        }
    }
    
    private static void sendBookInformation(Player player, BookMeta meta) {
        INFO.sendList(player, 2, "Book:");
        INFO.sendList(player, 3, "Title: " + (meta.hasTitle()? meta.getTitle() : "unnamed"));
        INFO.sendList(player, 3, "Author: " + (meta.hasAuthor()? meta.getAuthor() : "unsigned"));
        INFO.sendList(player, 3, "Pages: " + meta.getPageCount());
    }
    
    private static void sendLeatherArmorInformation(Player player, LeatherArmorMeta meta) {
        INFO.sendList(player, 2, "Leather color: " + colorToString(meta.getColor()));
    }
    
    private static void sendFireworkInformation(Player player, FireworkMeta meta) {
        INFO.sendList(player, 2, "Firework:");
        INFO.sendList(player, 3, "Power: " + meta.getPower());
        INFO.sendList(player, 3, "Effects:");
        for (FireworkEffect effect : meta.getEffects()) {
            INFO.sendList(player, 4, "");
            sendFireworkEffect(player, 5, effect);
        }
    }
    
    private static void sendFireworkStarInformation(Player player, FireworkEffectMeta meta) {
        INFO.sendList(player, 2, "Firework star:" + (meta.hasEffect()? "" : " empty"));
        if (meta.hasEffect()) {
            sendFireworkEffect(player, 3, meta.getEffect());
        }
    }
    
    private static void sendFireworkEffect(Player player, int level, FireworkEffect effect) {
        INFO.sendList(player, level, "Type: " + StringUtil.capitalizeFirst(effect.getType().name()));
        INFO.sendList(player, level, "Colors:");
        for (Color color : effect.getColors()) {
            INFO.sendList(player, level + 1, fireworkColorToString(color));
        }
        INFO.sendList(player, level, "Fade:");
        for (Color color : effect.getFadeColors()) {
            INFO.sendList(player, level + 1, fireworkColorToString(color));
        }
    }
    
    private static String fireworkColorToString(Color color) {
        DyeColor dye = DyeColor.getByFireworkColor(color);
        if (dye != null) {
            return dyeColorToString(dye);
        }
        return colorToRBGString(color);
    }
    
    private static String colorToString(Color color) {
        DyeColor dye = DyeColor.getByColor(color);
        if (dye != null) {
            return dyeColorToString(dye);
        }
        return colorToRBGString(color);
    }
    
    private static String dyeColorToString(DyeColor color) {
        return StringUtil.capitalizeFirst(color.name());
    }
    
    private static String colorToRBGString(Color color) {
        return String.format("Red:%d Green:%d Blue:%d", color.getRed(), color.getGreen(), color.getBlue());
    }

}
