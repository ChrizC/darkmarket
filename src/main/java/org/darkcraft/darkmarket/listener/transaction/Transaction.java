package org.darkcraft.darkmarket.listener.transaction;

import java.util.List;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.darkcraft.darkmarket.DarkMarket;
import org.darkcraft.darkmarket.Vault;
import org.darkcraft.darkmarket.event.TransactionEvent;
import org.darkcraft.darkmarket.utils.InventoryUtil;

import static org.darkcraft.darkmarket.event.TransactionEvent.TransactionType.BUY;
import static org.darkcraft.darkmarket.event.TransactionEvent.TransactionType.SELL;
import static org.darkcraft.darkmarket.utils.PriceUtil.FREE;

public class Transaction implements Listener {

    @EventHandler
    public void onShopItemRemover(TransactionEvent event) {
        if (event.getShop().isAdminShop() || event.getType() != BUY) {
            return;
        }

        List<Inventory> inventories = event.getShop().getInventory().getInventories();
        InventoryUtil.removeItem(inventories, event.getItem(), event.getQuantity());
    }

    @EventHandler
    public void onShopItemDeposit(TransactionEvent event) {
        if (event.getShop().isAdminShop() || event.getType() != SELL) {
            return;
        }

        List<Inventory> inventories = event.getShop().getInventory().getInventories();
        InventoryUtil.addItem(inventories, event.getItem(), event.getQuantity());
    }

    @EventHandler
    public void onPlayerItemRemover(TransactionEvent event) {
        if (event.getType() != SELL) {
            return;
        }

        InventoryUtil.removeItem(event.getClient().getInventory(), event.getItem(), event.getQuantity());
        event.getClient().updateInventory();
    }

    @EventHandler
    public void onPlayerItemDeposit(TransactionEvent event) {
        if (event.getType() != BUY) {
            return;
        }

        InventoryUtil.addItem(event.getClient().getInventory(), event.getItem(), event.getQuantity());
        event.getClient().updateInventory();
    }

    @EventHandler
    public void onEconomyWithdrawal(TransactionEvent event) {
        double price = event.getPrice();

        if (price == FREE) {
            return;
        }

        if (event.getType() == BUY) {
            Vault.getEconomy().withdrawPlayer(event.getClient().getName(), price);
        } else if (!event.getShop().isAdminShop()) {
            Vault.getEconomy().withdrawPlayer(event.getShop().getOwner().getName(), price);
        }
    }

    @EventHandler
    public void onEconomyDeposit(TransactionEvent event) {
        double price = event.getPrice();

        if (price == FREE) {
            return;
        }

        if (event.getType() == BUY) {
            if (event.getShop().isAdminShop()) {
                return;
            }
            Vault.getEconomy().depositPlayer(event.getShop().getOwner().getName(), price);
        } else {
            Vault.getEconomy().depositPlayer(event.getClient().getName(), price);
        }
    }
    
}
