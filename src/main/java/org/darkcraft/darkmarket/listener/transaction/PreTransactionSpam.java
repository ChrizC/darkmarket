package org.darkcraft.darkmarket.listener.transaction;

import java.util.Map;
import java.util.WeakHashMap;
import org.bukkit.event.Listener;
import org.darkcraft.darkmarket.event.PreTransactionEvent;

import static org.bukkit.event.EventPriority.LOWEST;

import org.bukkit.event.EventHandler;
import org.darkcraft.darkmarket.Config;
import org.darkcraft.darkmarket.event.PreTransactionEvent.State;

public class PreTransactionSpam implements Listener {
    
    private Map<String, Long> LAST_USE = new WeakHashMap();
    
    @EventHandler(priority = LOWEST)
    public void onPreTransaction(PreTransactionEvent e) {
        String client = e.getClient().getName();
        
        if (LAST_USE.containsKey(client) && System.currentTimeMillis() - LAST_USE.get(client) < Config.spamPreventWait) {
            e.setState(State.SPAM_PREVENTION);
            return;
        }
        
        LAST_USE.put(client, System.currentTimeMillis());
    }

}
