package org.darkcraft.darkmarket.listener.transaction;

import org.bukkit.OfflinePlayer;
import org.bukkit.event.EventHandler;
import org.darkcraft.darkmarket.event.TransactionEvent;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.darkcraft.darkmarket.Message;
import org.darkcraft.darkmarket.Vault;
import org.darkcraft.darkmarket.utils.ItemUtil;

import static org.darkcraft.darkmarket.event.TransactionEvent.TransactionType.BUY;
import static org.darkcraft.darkmarket.event.TransactionEvent.TransactionType.SELL;
import static org.bukkit.event.EventPriority.MONITOR;

public class TransactionMessenger implements Listener {
    
    private static final String CLIENT_BOUGHT = "You bought %s from %s for %s.";
    private static final String CLIENT_SOLD = "You sold %s to %s for %s.";
    
    private static final String SHOP_BOUGHT = "%s sold %s to you for %s.";
    private static final String SHOP_SOLD = "%s bought %s from you for %s.";    
        
    @EventHandler(priority = MONITOR)
    public void onBuyTransaction(TransactionEvent event) {
        if (event.getType() != BUY) {
            return;
        }
        
        String item = formatItem(event.getItem(), event.getQuantity());
        String price = Vault.getEconomy().format(event.getPrice());
        
        Message.INFO.send(event.getClient(), String.format(CLIENT_BOUGHT, item, event.getShop().getOwnerName(), price));
        
        OfflinePlayer owner = event.getShop().getOwner();
        if (event.getShop().isAdminShop() || !owner.isOnline()) {
            return;
        }
        
        Message.INFO.send(owner.getPlayer(), String.format(SHOP_SOLD, event.getClient().getName(), item, price));
    }
    
    @EventHandler(priority = MONITOR)
    public void onSellTransaction(TransactionEvent event) { 
        if (event.getType() != SELL) {
            return;
        }

        String item = formatItem(event.getItem(), event.getQuantity());
        String price = Vault.getEconomy().format(event.getPrice());
        
        Message.INFO.send(event.getClient(), String.format(CLIENT_SOLD, item, event.getShop().getOwnerName(), price));

        OfflinePlayer owner = event.getShop().getOwner();
        if (event.getShop().isAdminShop() || !owner.isOnline()) {
            return;
        }
        
        Message.INFO.send(owner.getPlayer(), String.format(SHOP_BOUGHT, event.getClient().getName(), item, price));
    }
    
    public static String formatItem(ItemStack item, int amount) {
        return amount + " " + ItemUtil.getItemName(item);
    }

}
