package org.darkcraft.darkmarket.listener.transaction;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import org.darkcraft.darkmarket.DarkMarket;
import org.darkcraft.darkmarket.db.Transactions;
import org.darkcraft.darkmarket.event.TransactionEvent;

import static org.darkcraft.darkmarket.event.TransactionEvent.TransactionType.BUY;
import static org.darkcraft.darkmarket.listener.transaction.TransactionMessenger.formatItem;
import static org.bukkit.event.EventPriority.MONITOR;

public class TransactionLogger implements Listener {

    public static final String BUY_MESSAGE = "%s bought %s from %s for %.2f at %s";
    public static final String SELL_MESSAGE = "%s sold %s to %s for %.2f at %s";

    @EventHandler(priority = MONITOR)
    public void onTransactionToConsole(TransactionEvent event) {
        String shopOwner = event.getShop().getOwnerName();
        String client = event.getClient().getName();
        String item = formatItem(event.getItem(), event.getQuantity());
        double price = event.getPrice();
        String location = locationToString(event.getShop().getSign().getLocation());

        String format = event.getType() == BUY ? BUY_MESSAGE : SELL_MESSAGE;

        DarkMarket.LOG.info(String.format(format, client, item, shopOwner, price, location));
    }

    @EventHandler(priority = MONITOR)
    public void onTransactionToDb(TransactionEvent event) {
        Transactions.logTransaction(event);
    }

    public static String locationToString(Location location) {
        return "[" + location.getWorld().getName() + ": " + location.getBlockX() + ", " + location.getBlockY() + ", " + location.getBlockZ() + "]";
    }
}
