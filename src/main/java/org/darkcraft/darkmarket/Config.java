package org.darkcraft.darkmarket;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import org.bukkit.configuration.file.YamlConfiguration;

public class Config {
    
    private static File configFile;
    
    public static String mysqlHostName  = "localhost";
    public static String mysqlPort      = "3306";
    public static String mysqlUsername  = "root";
    public static String mysqlPassword  = "";
    public static String mysqlDatabase  = "";
    
    public static int maxLinkedChests   = 15;
    public static int chestRadius       = 500;
    public static String shopHeader     = "[Shop]";
    public static String adminShopHeader= "[ATM]";
    public static boolean shiftSellsStack = true;
    public static int maximumStacks     = 1;
    
    public static boolean ignoreRepairCost = true;
    
    public static long spamPreventWait = 250;
    
    public static int timeBetweenQueueRun = 5;
    public static boolean debug = false;
    
    private static YamlConfiguration config;
    
    public static void init(File file) {
        configFile = file;
        
        config = YamlConfiguration.loadConfiguration(file);
        load();
        
        try {
            config.save(file);
        } catch (IOException e) {}
    }
    
    public static void reload() {
        config = YamlConfiguration.loadConfiguration(configFile);
    }
    
    private static void load() {
        Class cls = Config.class;
        try {
            set("mysql.host", cls.getField("mysqlHostName"));
            set("mysql.port", cls.getField("mysqlPort"));
            set("mysql.username", cls.getField("mysqlUsername"));
            set("mysql.password", cls.getField("mysqlPassword"));
            set("mysql.database", cls.getField("mysqlDatabase"));
            
            set("shop.chest-limit", cls.getField("maxLinkedChests"));
            set("shop.chest-radius", cls.getField("chestRadius"));
            set("shop.shop-header", cls.getField("shopHeader"));
            set("shop.admin-shop-header", cls.getField("adminShopHeader"));
            set("shop.shift-sells-stacks", cls.getField("shiftSellsStack"));
            set("shop.maximum-quantity-stacks", cls.getField("maximumStacks"));
            
            set("item.ignore-repair-cost", cls.getField("ignoreRepairCost"));
            
            set("transaction.spam-prevention-wait", cls.getField("spamPreventWait"));
            
            set("advanced.queue-execution-delay", cls.getField("timeBetweenQueueRun"));
            set("advanced.debug", cls.getField("debug"));
        } catch (NoSuchFieldException ex) {
            ex.printStackTrace();
        }
    }
    
    private static void set(String path, Field field) {
        //field.setAccessible(true);
        try {
            field.set(null, config.get(path, field.get(null)));
            config.set(path, field.get(null));
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
    }
    
}
