package org.darkcraft.darkmarket.chest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.darkcraft.darkmarket.db.Chests;
import org.darkcraft.darkmarket.shop.Shop;
import org.darkcraft.darkmarket.shop.ShopManager;

public class ShopChest {
    
    public static boolean isShopChest(Block chest) {
        return Chests.isShopChest(chest);
    }
    
    public static List<Shop> getShopsByChest(Block chest) {
        List<Integer> ids = Chests.getShopIdsByChest(chest);
        
        if (ids.isEmpty()) {
            return Collections.emptyList();
        }
        
        List<Shop> shops = new ArrayList();
        for (int id : ids) {
            Shop shop = ShopManager.getShopById(id);
            if (shop == null) {
                continue;
            }
            
            shops.add(shop);
        }
        return shops;
    }
    
    public static boolean canPlayerAccess(OfflinePlayer player, Block block) {
        List<Shop> shops = getShopsByChest(block);
        if (shops.isEmpty()) {
            return true;
        }
        
        for (Shop shop : shops) {
            if (shop.getOwnerName().equals(player.getName())) {
                return true;
            }
        }
        return false;
    }
    
}
