package org.darkcraft.darkmarket.chest;

import org.darkcraft.darkmarket.inventory.MultiChestInventory;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.darkcraft.darkmarket.Config;
import org.darkcraft.darkmarket.DarkMarket;
import org.darkcraft.darkmarket.db.Chests;
import org.darkcraft.darkmarket.shop.Shop;
import org.darkcraft.darkmarket.utils.uBlock;

public class ChestLinker {
    
    private Shop shop;
    private Set<Location> chests;
    
    public ChestLinker(Shop shop) {
        this(shop, shop.hasInventory()? new HashSet(shop.getInventory().getChests()) : new HashSet());
    }
    
    public ChestLinker(Shop shop, Set<Location> chests) {
        this.shop = shop;
        this.chests = chests;
    }
    
    public Result addChest(Block block) {
        Block toAdd = block;
        
        Block partner = uBlock.getChestPair(toAdd);
        if (!chests.contains(toAdd.getLocation()) && partner != null) {
            toAdd = partner;
        }
        
        if (isOutOfBounds(toAdd)) {
            return Result.OUT_OF_RANGE;
        }
        
        if (!DarkMarket.isLockedChestOwner(shop.getOwner(), toAdd) || (ShopChest.isShopChest(toAdd) && !ShopChest.canPlayerAccess(shop.getOwner(), block))) {
            return Result.CHEST_OWNED;
        }
        
        Location loc = toAdd.getLocation();
        if (chests.contains(loc)) {
            chests.remove(loc);
            return Result.REMOVED;
        } else {
            if (chests.size() >= Config.maxLinkedChests) {
                return Result.TOO_MANY_CHESTS;
            }
            chests.add(loc);
            return Result.ADDED;
        }
    }
    
    public void finish() {
        List<Location> old = shop.hasInventory()? shop.getInventory().getChests() : new ArrayList();
        MultiChestInventory inventory = new MultiChestInventory(chests);
        
        shop.setInventory(inventory);
        
        if (old.equals(chests)) {
            return;
        }
        
        Chests.updateShopChests(shop, old, chests);
    }
    
    public int getChestCount() {
        return chests.size();
    }
    
    public enum Result {
        ADDED,
        REMOVED,
        
        CHEST_OWNED,
        OUT_OF_RANGE,
        
        TOO_MANY_CHESTS,
    }
    
    private boolean isOutOfBounds(Block loc) {
        Location sloc = shop.getSign().getLocation();
        if (sloc.getWorld() != loc.getWorld()) return true;
        int x = Math.abs(sloc.getBlockX() - loc.getX());
        int z = Math.abs(sloc.getBlockZ() - loc.getZ());
        return (x > Config.chestRadius || z > Config.chestRadius);
    }
    
}
