package org.darkcraft.darkmarket;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public enum Message {
    
    INFO(ChatColor.GREEN, "[Shop]"),
    WARNING(ChatColor.YELLOW, "[Shop]"),
    ERROR(ChatColor.RED, "[Shop]");
    
    private ChatColor color;
    private String prefix;
    
    private Message(ChatColor color, String prefix) {
        this.color = color;
        this.prefix = prefix + ChatColor.RESET + " ";
    }
    
    public String getPrefix() {
        return prefix;
    }
    
    public ChatColor getColor() {
        return color;
    }
    
    public void send(CommandSender sender, String message) {
        sender.sendMessage(color + prefix + message);
    }
    
    public void send(CommandSender sender, String message, Object... args) {
        sender.sendMessage(color + prefix + String.format(message, args)); 
    }
    
    public void sendList(CommandSender sender, int depth, String message) {
        sender.sendMessage(whitespace(depth) + color + "- " + ChatColor.RESET + message);
    }
    
    private static String whitespace(int depth) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < depth; i++) {
            sb.append("  ");
        }
        return sb.toString();
    }
    
}
