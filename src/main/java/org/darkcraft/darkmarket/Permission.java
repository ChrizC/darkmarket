package org.darkcraft.darkmarket;

import org.bukkit.command.CommandSender;

public enum Permission {
    
    CREATE_SHOP("darkmarket.create"),
    CREATE_SHOP_ADMIN("darkmarket.create.admin"),
    
    USE_SHOP("darkmarket.use"),
    
    MOD("darkmarket.mod"),
    ADMIN("darkmarket.admin")
    ;
    
    private String permission;
    
    private Permission(String permission) {
        this.permission = permission;
    }
    
    public boolean has(CommandSender s) {
        return s.isOp() || s.hasPermission(permission);
    }
    
    @Override
    public String toString() {
        return permission;
    }

}
