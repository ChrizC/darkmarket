package org.darkcraft.darkmarket.shop;

import org.bukkit.block.Block;
import org.bukkit.block.Sign;

import java.util.regex.Pattern;
import org.bukkit.inventory.ItemStack;
import org.darkcraft.darkmarket.Config;
import org.darkcraft.darkmarket.utils.ItemUtil;
import org.darkcraft.darkmarket.utils.PriceUtil;
import org.darkcraft.darkmarket.utils.uBlock;

/**
 * @author Acrobot
 */
public class ShopSign {
    public static final int HEADER_LINE = 0;
    public static final int ITEM_LINE = 1;
    public static final int QUANTITY_LINE = 2;
    public static final int PRICE_LINE = 3;

    public static final Pattern[] SHOP_SIGN_PATTERN = {
            Pattern.compile("^\\[[\\w ]{1,13}\\]$"),
            Pattern.compile("^[\\w ]*$"),
            Pattern.compile("^\\d*$"),
            Pattern.compile("(?i)^[\\d.bs(free) :]*$")
    };

    public static boolean isValid(String[] lines, int line) {
        return SHOP_SIGN_PATTERN[line].matcher(lines[line]).matches();
    }
    
    public static boolean isValid(String text, int line) {
        return SHOP_SIGN_PATTERN[line].matcher(text).matches();
    }
    
    public static boolean isValid(Block sign) {
        return uBlock.isSign(sign) && isValid((Sign) sign.getState());
    }

    public static boolean isValid(Sign sign) {
        return isValid(sign.getLines());
    }

    public static boolean isValid(String[] line) {
        return isValidPreparedSign(line) && (line[PRICE_LINE].toUpperCase().contains("B") || line[PRICE_LINE].toUpperCase().contains("S")) && isHeaderValid(line[HEADER_LINE]);
    }

    public static boolean isValidPreparedSign(String[] lines) {
        for (int i = 0; i < 4; i++) {
            if (!SHOP_SIGN_PATTERN[i].matcher(lines[i]).matches()) {
                return false;
            }
        }
        return lines[PRICE_LINE].indexOf(':') == lines[PRICE_LINE].lastIndexOf(':');
    }
    
    public static boolean isHeaderValid(String text) {
        return Config.shopHeader.equalsIgnoreCase(text) || isAdminShop(text);
    }
    
    public static boolean isAdminShop(Block block) {
        return uBlock.isSign(block) && isAdminShop((Sign) block.getState());
    }
    
    public static boolean isAdminShop(String line) {
        return Config.adminShopHeader.equalsIgnoreCase(line);
    }
    
    public static boolean isAdminShop(Sign sign) {
        return isAdminShop(sign.getLine(HEADER_LINE));
    }
    
    /**
     * Formats a sign for a shop
     * @param force create a sign if one didn't exist
     */
    public static boolean formatShopSign(Shop shop, boolean force) {
        Block block = shop.getSign();
        if (!uBlock.isSign(block)) {
            if (!force) {
                return false;
            }
            
            uBlock.setSign(block);
            
            if (!uBlock.isSign(block)) {
                return false;
            }
        }
        
        Sign sign = (Sign) block.getState();
        String[] lines = getShopSignFormat(shop);
        
        for (int i = 0; i < lines.length; i++) {
            sign.setLine(i, lines[i]);
        }
        
        return sign.update(true);
    }
    
    /**
     * Returns String[] of formatted lines for a shop sign
     */
    public static String[] getShopSignFormat(Shop shop) {
        return getShopSignFormat(shop.isAdminShop(), shop.getItem(), shop.getQuantity(), shop.getBuyPrice(), shop.getSellPrice());
    }
    
    public static String[] getShopSignFormat(boolean admin, ItemStack item, int quantity, double buy, double sell) {
        String[] lines = new String[4];
        lines[HEADER_LINE] = admin? Config.adminShopHeader : Config.shopHeader;
        lines[ITEM_LINE] = ItemUtil.getItemSignName(item);
        lines[QUANTITY_LINE] =  String.valueOf(quantity);
        lines[PRICE_LINE] = PriceUtil.getSignFormat(buy, sell);
        return lines;
    }
    
}
