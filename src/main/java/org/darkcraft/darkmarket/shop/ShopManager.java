package org.darkcraft.darkmarket.shop;

import java.util.HashMap;
import java.util.Map;
import org.bukkit.Location;
import org.darkcraft.darkmarket.db.Chests;
import org.darkcraft.darkmarket.db.Shops;
import org.darkcraft.darkmarket.event.ShopCreationEvent;

public class ShopManager {
    
    private static final Map<Location, Shop> SHOPS = new HashMap();
    
    public static boolean isShop(Location location) {
        if (SHOPS.containsKey(location)) {
            return true;
        }
        
        return getShop(location) != null;
    }
    
    public static Shop getShop(Location location) {
        if (SHOPS.containsKey(location)) {
            return SHOPS.get(location);
        }
        
        Shop shop = Shops.getByLocation(location);
        
        if (shop != null) {
            SHOPS.put(location, shop);
            Chests.loadShopChests(shop);
        }
        
        return shop;
    }
    
    public static Shop getShopById(int id) {
        for (Shop shop : SHOPS.values()) {
            if (shop.getId() == id) {
                return shop;
            }
        }
        
        Location location = Shops.getShopLocation(id);
        
        if (location == null) {
            return null;
        }
        
        return getShop(location);
    }
    
    public static Shop createShop(ShopCreationEvent event) {
        Shop shop = Shops.createShop(event);
        
        if (shop != null) {
            SHOPS.put(shop.getSign().getLocation(), shop);
        }
        
        return shop;
    }
    
    public static void deleteShop(Shop shop) {
        SHOPS.remove(shop.getSign().getLocation());
        Shops.deleteShop(shop);
        shop.getSign().breakNaturally();
    }

}
