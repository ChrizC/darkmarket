package org.darkcraft.darkmarket.shop;

import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import org.darkcraft.darkmarket.Config;
import org.darkcraft.darkmarket.inventory.MultiChestInventory;

public class Shop {
    
    private final int id;
    
    private OfflinePlayer owner;
    
    private Block sign;
    
    private ItemStack item;
    private int quantity;
    
    private double buyPrice;
    private double sellPrice;
    
    private MultiChestInventory inventory = null;
    
    private boolean admin;
    
    public Shop(OfflinePlayer owner, Block sign, ItemStack item, int quantity, double buyPrice, double sellPrice) {
        this(-1, owner, sign, item, quantity, buyPrice, sellPrice);
    }
    
    public Shop(int id, OfflinePlayer owner, Block sign, ItemStack item, int quantity, double buyPrice, double sellPrice) {
        this(id, owner, ShopSign.isAdminShop(sign), sign, item, quantity, buyPrice, sellPrice);
    }
    
    public Shop(int id, OfflinePlayer owner, boolean admin, Block sign, ItemStack item, int quantity, double buyPrice, double sellPrice) {
        this.id = id;
        this.owner = owner;
        this.admin = admin;
        this.sign = sign;
        this.item = item;
        this.quantity = quantity;
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
    }
    
    public int getId() {
        return id;
    }
    
    public OfflinePlayer getOwner() {
        return owner;
    }
    
    public void setOwner(OfflinePlayer owner) {
        this.owner = owner;
    }
    
    public String getOwnerName() {
        return admin? Config.adminShopHeader : owner.getName();
    }
    
    public boolean isOwner(OfflinePlayer player) {
        return !admin && owner.getName().equals(player.getName());
    }
    
    public Block getSign() {
        return sign;
    }
    
    public ItemStack getItem() {
        return item;
    }
    
    public void setItem(ItemStack item) {
        this.item = item;
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public double getBuyPrice() {
        return buyPrice;
    }
    
    public void setBuyPrice(double price) {
        this.buyPrice = price;
    }
    
    public double getSellPrice() {
        return sellPrice;
    }
    
    public void setSellPrice(double price) {
        this.sellPrice = price;
    }
    
    public MultiChestInventory getInventory() {
        return inventory;
    }
    
    public void setInventory(MultiChestInventory inventory) {
        this.inventory = inventory;
    }
    
    public boolean isAdminShop() {
        return admin;
    }
    
    public void setAdminShop(boolean admin) {
        this.admin = admin;
    }
    
    public boolean hasInventory() {
        return inventory != null && !inventory.getChests().isEmpty();
    }

}
