package org.darkcraft.darkmarket.inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.darkcraft.darkmarket.utils.InventoryUtil;

public class MultiInventory implements Inventory {
    
    private static final int INITIAL_SIZE = 10;
    
    private final List<Inventory> Inventories;
    
    public MultiInventory() {
        this(INITIAL_SIZE);
    }
    
    public MultiInventory(int size) {
        this.Inventories = new ArrayList(size);
    }
    
    public MultiInventory(List<Inventory> chests) {
        this.Inventories = chests;
    }
    
    public List<Inventory> getInventories() {
        return Collections.unmodifiableList(Inventories);
    }
    
    public boolean addChest(Chest chest) {
        Inventory inventory = chest.getInventory();
        
        if (!canAdd(inventory)) {
            return false;
        }
        
        if (Inventories.add(inventory)) {
            size += inventory.getSize();
            return true;
        }
        return false;
    }
    
    public boolean removeChest(Chest chest) {
        Inventory inventory = chest.getInventory();
        if (Inventories.remove(inventory)) {
            size -= inventory.getSize();
            return true;
        }
        return false;
    }
    
    public boolean canAdd(Inventory inventory) {
        return !Inventories.contains(inventory);
    }
    
    public boolean removeAll(Collection<? extends Inventory> inventories) {
        return Inventories.removeAll(inventories);
    }
    
    public boolean addAll(Collection<? extends Inventory> inventories) {
        Collection<? extends Inventory> collection = new ArrayList(inventories);
        collection.removeAll(Inventories);
        return Inventories.addAll(collection);
    }
    
    private int size = -1;

    @Override
    public int getSize() {
        if (size == -1) {
            size = countTotal();
        }
        return size;
    }
    
    private int countTotal() {
        int i = 0;
        for (Inventory inventory : Inventories) {
            i += inventory.getSize();
        }
        return i;
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public ItemStack getItem(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException(String.format("Out of bounds (%d)", index));
        }
        
        int i = 0;
        for (Inventory inv : Inventories) {
            if ((i += inv.getSize()) <= index && i + inv.getSize() > index) {
                return inv.getItem(index - i);
            }
        }
        return null;
    }

    @Override
    public void setItem(int index, ItemStack item) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException(String.format("Out of bounds (%d)", index));
        }
        
        int i = 0;
        for (Inventory inv : Inventories) {
            if ((i += inv.getSize()) <= index && i + inv.getSize() > index) {
                inv.setItem(index - i, item);
            }
        }
    }

    @Override
    public HashMap<Integer, ItemStack> addItem(ItemStack... items) throws IllegalArgumentException {
        HashMap<Integer, ItemStack> leftOver = Inventories.get(0).addItem(items);
        
        for (int i = 1, k = Inventories.size(); i < k; i++) {
            Inventory inv = Inventories.get(i);

            if (leftOver.isEmpty()) {
                break;
            }
                        
            Collection<ItemStack> left = leftOver.values();
            leftOver = inv.addItem(left.toArray(new ItemStack[left.size()]));
        }
        
        return leftOver;
    }

    @Override
    public HashMap<Integer, ItemStack> removeItem(ItemStack... items) throws IllegalArgumentException {
        HashMap<Integer, ItemStack> remaining = Inventories.get(0).removeItem(items);
        
        for (int i = 1, k = Inventories.size(); i < k; i++) {
            Inventory inv = Inventories.get(i);
            
            if (remaining.isEmpty()) {
                break;
            }
            
            Collection<ItemStack> left = remaining.values();
            remaining = inv.removeItem(left.toArray(new ItemStack[left.size()]));
        }
        
        return remaining;
    }
    
    @Override
    public ItemStack[] getContents() {
        ItemStack[] items = new ItemStack[size];
        
        int i = 0;
        for (Inventory inv : Inventories) {
            ItemStack[] contents = inv.getContents();
            i += contents.length;
            System.arraycopy(contents, 0, items, i, contents.length);
        }
        
        return items;
    }

    @Override
    public void setContents(ItemStack[] items) throws IllegalArgumentException {
        if (items.length < size) {
            throw new IllegalArgumentException("Array is too small.");
        }
        
        int i = 0;
        for (Inventory inv : Inventories) {
            ItemStack[] part = new ItemStack[inv.getSize()];
            System.arraycopy(items, i, part, 0, part.length);
            inv.setContents(part);
            i += inv.getSize();
        }
    }

    @Override
    public boolean contains(int materialId) {
        return contains(Material.getMaterial(materialId));
    }

    @Override
    public boolean contains(Material material) throws IllegalArgumentException {
        for (Inventory inv : Inventories) {
            if (inv.contains(material)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean contains(ItemStack item) {
        for (Inventory inv : Inventories) {
            if (inv.contains(item)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean contains(int materialId, int amount) {
        return contains(Material.getMaterial(materialId), amount);
    }

    @Override
    public boolean contains(Material material, int amount) throws IllegalArgumentException {
        for (Inventory inv : Inventories) {
            if (inv.contains(material, amount)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean contains(ItemStack item, int amount) {
        for (Inventory inv : Inventories) {
            if (inv.contains(item, amount)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAtLeast(ItemStack item, int amount) {
        return InventoryUtil.has(Inventories, item) >= amount;
    }

    @Override
    public HashMap<Integer, ? extends ItemStack> all(int materialId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public HashMap<Integer, ? extends ItemStack> all(Material material) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    @Override
    public HashMap<Integer, ? extends ItemStack> all(ItemStack item) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int first(int materialId) {
        int i = 0;
        for (Inventory inv : Inventories) {
            int k = inv.first(materialId);
            if (k > -1) {
                return i + k;
            }
            i += inv.getSize();
        }
        return -1;
    }

    @Override
    public int first(Material material) throws IllegalArgumentException {
        return first(material.getId());
    }

    @Override
    public int first(ItemStack item) {
        return -1;
    }

    @Override
    public int firstEmpty() {
        int i = 0;
        for (Inventory inv : Inventories) {
            int k = inv.firstEmpty();
            if (k > -1) {
                return i + k;
            }
            i += inv.getSize();
        }
        return -1;
    }

    @Override
    public void remove(int materialId) {
        for (Inventory inv : Inventories) {
            inv.remove(materialId);
        }
    }

    @Override
    public void remove(Material material) throws IllegalArgumentException {
        for (Inventory inv : Inventories) {
            inv.remove(material);
        }
    }

    @Override
    public void remove(ItemStack item) {
        for (Inventory inv : Inventories) {
            inv.remove(item);
        }
    }

    @Override
    public void clear(int index) {
        ItemStack item = getItem(index);
        
        if (item == null) {
            return;
        }
        
        setItem(index, null);
    }

    @Override
    public void clear() {
        for (Inventory inv : Inventories) {
            inv.clear();
        }
    }

    @Override
    public List<HumanEntity> getViewers() {
        return null;
    }

    @Override
    public String getTitle() {
        return "Multiple Inventory";
    }

    @Override
    public InventoryType getType() {
        return InventoryType.CHEST;
    }

    @Override
    public InventoryHolder getHolder() {
        return null;
    }

    @Override
    public ListIterator<ItemStack> iterator() {
        return iterator(0);
    }

    @Override
    public ListIterator<ItemStack> iterator(int index) {
        List<ItemStack> list = new ArrayList(size);
        for (Inventory inv : Inventories) {
            list.addAll(Arrays.asList(inv.getContents()));
        }
        return list.listIterator(index);
    }

    @Override
    public int getMaxStackSize() {
        return 0;
    }

    @Override
    public void setMaxStackSize(int size) {
    }

}
