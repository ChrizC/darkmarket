package org.darkcraft.darkmarket.inventory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.inventory.Inventory;
import org.darkcraft.darkmarket.utils.uBlock;

public class MultiChestInventory extends MultiInventory {
    
    private List<Location> chests;
    
    private MultiChestInventory() {
        this.chests = new ArrayList();
    }
    
    public MultiChestInventory(Set<Location> chests) {
        this(new ArrayList(chests));
    }
    
    public MultiChestInventory(List<Location> chests) {
        this.chests = new ArrayList();
        addAll(chests);
    }
    
    private void addAll(List<Location> locations) {
        for (Location location : locations) {
            Block block = location.getBlock();
            
            if (!uBlock.isChest(block)) {
                continue;
            }
            
            addChest((Chest) block.getState());
        }
    }
    
    public List<Location> getChests() {
        return chests;
    }
    
    public int getChestCount() {
        return chests.size();
    }
    
    @Override
    public boolean addChest(Chest chest) {
        if (super.addChest(chest)) {
            add(chest.getLocation());
            return true;
        }
        return false;
    }
    
    @Override
    public boolean removeChest(Chest chest) {
        if (super.removeChest(chest)) {
            remove(chest.getLocation());
            return true;
        }
        return false;
    }
    
    @Override
    public boolean addAll(Collection<? extends Inventory> inventories) {
        for (Iterator<? extends Inventory> iter = inventories.iterator(); iter.hasNext();) {
            Inventory inv = iter.next();

            if (!(inv.getHolder() instanceof Chest || inv.getHolder() instanceof DoubleChest)) {
                iter.remove();
                continue;
            }

            add(((BlockState) inv.getHolder()).getLocation());
        }
        return super.addAll(inventories);
    }
    
    @Override
    public boolean removeAll(Collection<? extends Inventory> inventories) {
        for (Iterator<? extends Inventory> iter = inventories.iterator(); iter.hasNext();) {
            Inventory inv = iter.next();
            
            if (!(inv.getHolder() instanceof Chest)) {
                iter.remove();
                continue;
            }

            remove(((Chest) inv.getHolder()).getLocation());
        }
        return super.removeAll(inventories);
    }
    
    private void add(Location loc) {
        chests.add(loc);
    }
    
    private void remove(Location loc) {
        chests.remove(loc);
    }

}
